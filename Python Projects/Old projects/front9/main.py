def front9(vect):
    dist = 0
    if len(vect) > 4:
        dist = 4
    else:
        dist = len(vect)
    for i in range(dist):
        if vect[i] == 9:
            return True
    return False


print(front9([2,7,3,4,2,34]))