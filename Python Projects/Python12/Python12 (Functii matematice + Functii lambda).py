# FUNCTII MATEMATICE
####################

import math

# min() si max() primesc ca parametrii o lista de numere
print(min(2, 5, 10))    # va afisa 2
print(max(2, 6, 10))    # va afisa 10

# floor() extrage partea intreaga inferior, iar ceil() extrage partea intreaga superior a unui numar real
print(math.floor(23.83253)) # va afisa 23
print(math.ceil(23.83253))  # va afisa 24

# abs() returneaza valoarea absoluta
print(abs(45.6))    # va afisa 45.6
print(abs(-11))     # va afsa 11

# round() rotunjeste numerul, iar cel de-al 2-lea parametru reprezinta precizia (numarul exact de zecimale)
n = 345.923456
print(round(n))     # 346
print(round(n, 0))  # 346.0
print(round(n, 1))  # 345.9
print(round(n, 2))  # 345.92
print(round(n, 3))  # 345.923

# sum() poate fi utilizata pentru orice colecție iterabila, cum ar fi listele. 
# Daca lista va conține și valori ne-numerice (Exemplu: 3, 4, 5, t, 4), va fi semnalata o eroare.
valori = [0.234, 1, 2.34, 5, 89, 0.666]
print(sum(valori))  # 98.24

# pow(x, y) returneaza x la puterea y
x = pow(4, 3)
print(x)    # 64


############################################################################################################################


# Exercitii
# 1.
x = 2
ecuatia1 = sum([x, pow(x, 2)]) * 3
print(ecuatia1)

x = 3
y = 4
ecuatia2 = x * sum([y, pow(x, 2), pow(2, 3)])
print (ecuatia2)

x = 5
y = 7
ecuatia3 = 5 - sum([x, y, y * pow(4, 2)])
print(ecuatia3)


# 2.
a = [4, 7, 6, 9, 5, 8, 3, 6]
print(min(a))
print(max(a))


# 3.
b = [-4, -192, 5, -5, 334, 0, -54]
c = []
for elem in b:
    if elem < 0:
        c.append(abs(elem))
print(c)


# 4.
d = [34.5, 7, 222.222, 98, 23.45, 578.2435]
for elem in d:
    print(math.floor(elem), end=' ')
print()


############################################################################################################################


# FUNCTII LAMBDA (functii anonime)
##################################

# Aceasta poate lua orice număr de argumente, dar poate avea o singură expresie.
# Sintaxa:  lambda arguments : expression 
x = lambda a : a + 10   # adauga 10 numarului a
print(x(5))     # 15
x = lambda a, b : a * b
print(x(5, 6))  # 30


############################################################################################################################


# Exercitii
# 1. 
suma = lambda a, b, c : a + b + c
print(suma(3, 4, 5))

# 2.
diff = lambda a, b, c : a - b - c
print(diff(8, 19, 10))

# 3.
mul = lambda a, b : a * b
print("Tabla inmultirii cu 5")
for i in range(11):
    print(f"5 x {i} = {mul(5, i)}")

import random
nr = random.randint(0, 10)
print(f"Tabla inmultirii cu {nr}")
for i in range(11):
    print(f"{nr} x {i} = {mul(nr, i)}")

nr = random.randint(0, 10)
print(f"Tabla inmultirii cu {nr}")
for i in range(11):
    print(f"{nr} x {i} = {mul(nr, i)}")


############################################################################################################################


# TEMA
# 1.
x = 5
ec1 = pow(x, 3) * 3
print(ec1)

ec2 = sum([2 * x, 3 * pow(x, 3)])
print(ec2)

y = 3
ec3 = sum([10, 5 * x, x * y * 4])
print(ec3)


# 2.
# a
v = [4.123, 7, 6, 20.4, -23, -5, 3.56, 9, 5, 8.78, 3, -6, -11, 2.23, -2, 4]
v1 = []
for i in range(len(v)):
    if v[i] < 0:
        v1.append(v[i])
print(v1)

# b
v2 = []
for i in range(len(v)):
    if type(v[i]) is float:
        v2.append(v[i])
print(v2)

# c
print(max(v1))

# d
print(min(v2))

# e
v3 = []
for i in range(len(v)):
    if v[i] not in v1 and v[i] not in v2:
        v3.append(v[i])
print(v2)


# 3
minim = lambda a, b, c : min(a, b, c)
print(minim(1, 2, 3))

# 4
mul3 = lambda a, b, c : a * b * c
print(mul3(2, 3, 4))