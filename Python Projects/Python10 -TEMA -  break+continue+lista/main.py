# liste exercitiu
numere = [0, 1, 2, 3, 4, 5]
print(numere)
numere.extend([6,7,8,9])
numere.pop(2)
del numere[5]
del numere[7]
print(numere)
if(2 in numere or 7 in numere):
    print(True)

for i in range(len(numere)):
    print(numere[i])


# break+continue
n = int(input("un numar de la 1 la 30"))
for i in range(0, 30, 1):
    if (i % 2 == 0):
        continue
    if (i == n):
        break
    print(i)