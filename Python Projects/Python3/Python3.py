# Aplicatii
# Creeaza o variabila numar.
x = 10

# Daca numarul este par afiseaza-l, daca nu afiseaza “Numarul nu este par”
if x % 2 == 0:
    print(x)
else:
    print("Numarul nu este par")

# Afiseaza acel numar ca numar intreg iar apoi afiseaza-l ca numar cu virgula.
print(f"Intreg: {x}, cu virgula: {float(x)}")

# Verifica daca numarul acela este mai mare ca 0. Daca e mai mare ca 0 afiseaza-l , daca nu aduna-l cu 100.
if x > 0:
    print(x)
else:
    print(x + 100)



# Liste
# Exercitiu
l = [0, 1, 2, 3, 4, 5]
for i in range(6, 10):
    l.append(i)
print(l)
del l[2]
del l[5]
del l[7]
print(l)
check = 3 in l and 7 in l
print(check)
for elem in l:
    print(elem)



# Parcurgere si sortare lista
# Exercitiu
l = []
for i in range(0, 5):
    x = int(input("In introduceti un numar: "))
    l.append(x)
i = 0
while i < 5:
    x = int(input("In introduceti un numar: "))
    l.append(x)
    i += 1
print(l)
l.sort()
print(l)
l.sort(reverse=True)
print(l)



# Functii disponibile intr-o lista
l = []
l.insert(0, 5)   # insereaza 5 pe pozitia 0
l.append(1)      # adauga 1 la sfarsitul listei
print(f"Lista este: {l}")
print(f"Numarul de aparitii ale lui {1} in lista este: {l.count(1)}")
print(f"Indexul lui {1} in lista este: {l.index(1)}")
l.pop(0)        # sterge elementul de pe pozitia 0 
l.remove(1)     # sterge prima aparitie a lui 1 in lista
l.reverse()     # inverseaza oridinea elementelor din lista
l.sort()        # sorteaza
list = l.copy() # copiaza lista
l.clear()       # sterge toate elementele din lista



# Exersare
nume_sportivi = ["Tarnovanu", "Olaru", "Coman", "Cordea", "Compagno", "Omrani", "Octavian Popescu"]
sport_practicat = ["fotbal", "fotbal", "fotbal", "fotbal", "fotbal", "fotbal", "fotbal"]
palmares = ["Castigator turneu: -", "Castigator turneu: -", "Castigator turneu: -", "Castigator turneu: -", "Castigator turneu: -", "Castigator turneu: -", "Castigator turneu: -", ]

# 1.
x = int(input("Introduceti un numar: "))
print(f"Sportivul {nume_sportivi[x]} practica sportul {sport_practicat[x]} si este {palmares[x]}")

# 2.
nume = input("Introduceti un nume: ")
for i in range(len(nume_sportivi)):
    if nume_sportivi[i] == nume:
        print(nume_sportivi[i])
        print(palmares[i])
        print(sport_practicat[i])

# 3.
for i in range(len(nume_sportivi)):
    if nume_sportivi[i] == nume:
        break
    print(nume_sportivi[i])

# 4. 
for i in range(len(nume_sportivi)):
    if nume_sportivi[i] == nume:
        continue
    print(nume_sportivi[i])

# 5. & 6.
for i in range(len(nume_sportivi)):
    if nume_sportivi[i] == nume:
        print(f"Lungimea listei inainte de stergere este: {len(nume_sportivi)}")
        #
        del nume_sportivi[i]
        del palmares[i]
        del sport_practicat[i]
        #
        print(nume_sportivi)
        print(palmares)
        print(sport_practicat)
        #
        print(f"Lungimea listei dupa stergere este: {len(nume_sportivi)}")
        break



# Tema 
#1
oameni = ["Marcel", "Georgel", "Ion", "Mircea", "Pavel", "Ana", "Ana"]
#2
oameni.sort()
print(oameni)
#3
del oameni[5]
#4
print(len(oameni))
#5
if "Ion" in oameni:
    oameni.remove("Ion")    # sterge prima aparitie a lui Ion in lista
#6
print(len(oameni))
#7
print(oameni.count("Ana"))
#8
oameni.sort(reverse=True)
print(oameni)
#9
numele_oamenilor2 = oameni.copy()
print(numele_oamenilor2)
#10
oameni.clear()
print(oameni)



# Tema bonus
# 1
lista_oameni = ["Marcel", "Georgel", "Ion", "Mircea", "Pavel", "Ana", "Ana"]
lista_oameni2 = []
x = 0 
for i in lista_oameni:
    x = x + 1
    for j in range(x * x):
        if j < len(lista_oameni):
            lista_oameni2.append(lista_oameni[j])
print(lista_oameni)
print(lista_oameni2)

# 2 
l = []
for i in range(0, -100, -3):
    l.append(i)
print(l)

# 3
import random as r
l = [r.randint(0, 100) for i in range(0, 5)]
print(l)

# 4
maxx = 0
l = [r.randint(0, 100) for i in range(0, 5)]
print(l)
for elem in l:
    if elem > maxx:
        maxx = elem
print(maxx)