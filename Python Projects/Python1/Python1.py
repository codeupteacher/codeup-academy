# Aplicatie
# 1
text_salvat = 'Hello World'
print(text_salvat)

text_salvat = "Hello World"
print(text_salvat)

text_salvat = """Hello World"""
print(text_salvat)


# 2
IsAlpha = text_salvat.isalpha()   # tip de data caracter
IsDigit = text_salvat.isdigit()   # cifra; tip de data int
print(IsAlpha) # False
print(IsDigit) # False

# 3
print("codeup Academy".capitalize())    # Codeup academy;  capitalize - face ca primul caracter sa fie litera mare, iar restul litere mici
print("hello".replace("e", "a"))        # hallo



# Exercitiu
nume = "Arsene Marinel"
varsta = 22
print("Numele meu este {0} si am varsta de {1} ani".format(nume, varsta))



# Tipul Boolean
IsGreater = 5 > 3
print(IsGreater)

# Exercitiu
variabilaBooleana = 4 + 2 == 6 or 3 > 2 and 4 > 1
print(variabilaBooleana)



# Exemple input()
# Exemplu 1
text = input()  # variabila text poate sa primeasca orice fel de valori ( int, long, string, bool)
print(text)

# Typecasting the input to integer
num1 = int(input("Introdu un numar intreg pentru a calcula suma lui cu urmatorul: "))
num2 = int(input("Al doilea numar intreg din suma ce vrei sa fie calculata: "))
print(num1 + num2)

#Typecasting the input to string
text = str(input())
print(text)

#Typecasting the input to float
real1 = float(input())
real2 = float(input())
print(abs(real2 - real1))



# Bucle conditionale
if 4 > 5:
    print("Adevarat")
    print("4 este mai mare ca 5")

# Exercitiu
if 4 + 2 == 6 or 3 > 2 and 4 > 1:
    print("Ecuatia: 4 + 2 == 6 or 3 > 2 and 4 > 1")



# Exercitiul 1
a = int(input("Latura 1: "))
b = int(input("Latura 2: "))
c = int(input("Latura 3: "))
if a == b and b == c:
    print("Triunghiul este echilateral")
elif a == b or b == c or c == a:
    print("Triunghiul este isoscel")
else:
    print("Triunghiul este oarecare")



# Exercitiul 2
celcius = 100
fahrenheit = float(celcius * 9 / 5 + 32)
print(fahrenheit)



# Exercitiul 3
print("Lista de produse:")
print("1. Shaorma: 24 lei")
print("2. Pantofi: 240 lei")
print("3. Tricou: 12 lei")
ok = True
numar_produse = 0
lista_preturi = [24, 240, 12]
cost = 0
while ok:
    if numar_produse == 0:
        print("Doriti sa achizitionati vreun produs? (y/n)")
    else:
        print("Doriti sa achizitionati un alt produs? (y/n)")
    raspuns = input()
    
    if raspuns == "yes":
        produs = int(input("Introduceti numarul produsul din lista de produse: "))
        if produs > len(lista_preturi) or produs < 1:
            print("Produs invalid")
        else:
            numar_produse += 1
            cost += lista_preturi[produs - 1]
    elif raspuns == "no":
        ok = False
    else:
        print("Va rugam sa raspundeti cu yes or no!")
if cost > 100:
    cost = cost - cost * 0.1  # 10% reducere
print("Costul total al utilizatorului: ", cost)



# Tema
# 1
nume = input("Numele scolii: ")
numar = int(input("Numarul scolii: "))
media = float(input("Media de la capacitate: "))
if media > 8:
    print("Scoala {0}, numarul {1} are media {2} care este suficienta pentru a intra in top 100 scoli".format(nume, numar, media))
else:
    print("Scoala {0}, numarul {1} are media {2} care NU este suficienta pentru a intra in top 100 scoli".format(nume, numar, media))

# 2
a = int(input("Primul numar introdus: "))
b = int(input("Al doilea numar introdus: "))
if a > b:
    print("Primul numar introdus are valoarea {0}, este mai mare decat al 2-lea numar introdus, cu valoarea {1}".format(a, b))
else:
    print("Al 2-lea numar introdus are valoarea {1}, este mai mare decat primul numar introdus, cu valoarea {0}".format(a, b))