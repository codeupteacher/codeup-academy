# Lectia 11

# Recapitulare
v = [1, 2, 3, 4]
del v[:2]   # sterge tot ceea ce este inainte de pozitia 2
print(v)    # [3, 4]


######################################################################################


# Exercitii
# 1
def potDormi(isWeekday, isVacation):
    if isWeekday == False or isVacation == True:
        return True
    else:
        return False

print(f"Pot dormi? {potDormi(1, 0)}")

# 2
def suma_Dubla(numar1, numar2):
    return numar1 + numar2

numar1 = 3
numar2 = 3
print(f"Suma numerelor {numar1} si {numar2} este {suma_Dubla(numar1, numar2)}")

# 3
def addNot(string):
    if len(string) < 3:
        return "Error"
    elif string[:3] == "not":
        return string
    else:
        return "not" + string

string = "nothello"
print(addNot(string))

# 4
def multiplyString(varString, varInt):
    copyVarString = varString
    while varInt > 1:
        varString += copyVarString
        varInt -= 1
    return varString

varString = "Hi"
varInt = 2
print(f"String-ul {varString} multiplicat de {varInt} ori este: {multiplyString(varString, varInt)}")

######################################################################################


# Joc: Guess the number
# =====================
import random

num = random.randint(1, 10)
guess = None
attempts = 0

while num != guess: 
    guess = int(input("Guess a number between 1 and 10: "))
    attempts += 1
    if num == guess:
        print("You won")
        break
    if attempts == 3:
        print("You lost!")
        break
    

######################################################################################


# Exercitiul 2
# 5 
def eliminaCaracter(sir, poz):
    front = sir[:poz]
    back = sir[poz + 1:]
    print(front + back)
eliminaCaracter("mancare", 2)

# 6
def getEvenCharsFromString(sir):
    for i in range(0, len(sir), 2):
        print(sir[i])

sir = "Heeololeo"
getEvenCharsFromString(sir)

def getEvenCharsFromString1(sir):
    x = ""
    for i in range(0, len(sir), 2):
        x += sir[i]
    print(x)
getEvenCharsFromString1(sir)


# 7
def array_count9(v):
    nr = 0
    for i in v:
        if i == 9:
            nr +=1
    print(nr)

v = []
n = int(input("Numarul de elemente din vector: "))

for i in range(n):
    el = int(input())
    v.append(el)

array_count9(v)


# 8
def array_front9(v):
    nr = 4
    if len(v) < 4:
        nr = len(v)
    for i in range(0, nr):
        if v[i] == 9:
            return True
    return False

print(array_front9(v))


# 9
def array_123(vector):
    for i in range(len(vector) - 2):
        if vector[i] == 1 and vector[i + 1] == 2 and vector[i + 2] == 3:
            return True
    return False

vector = [4, 6, 1, 2, 3, 5, 7]
print(array_123(vector))


######################################################################################


# Tema
# 1
def selectPozitiiImpare(parametru_primit):
    string_impar = ""
    for i in range(len(parametru_primit)):
        if i % 2 == 1:
            string_impar += parametru_primit[i]
    return string_impar

print(selectPozitiiImpare("Ma duc la piata"))

# 2
def cateVocaleGasim(sir):
    contor = 0
    for i in range(len(sir)):
        if sir[i] == 'a' or sir[i] == 'e' or sir[i] == 'i' or sir[i] == 'o' or sir[i] == 'u':
            contor += 1
    return contor

print(cateVocaleGasim("Ma duc la piata"))


