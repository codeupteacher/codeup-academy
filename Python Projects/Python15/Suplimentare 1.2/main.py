#Ex 1
nr1 = int(20)
nr2 = float(10)
nume = "codeup"

restanta = 0
notaFinala = 10.0
laborator ="Programare Python"

print(type(restanta))
print(type(notaFinala))
print(type(laborator))

#Ex2

colegi_de_clasa = ["Istrate","Nicoara","Cerasela","Cristi","Filip","Nutu","Dumbrava","Istrate","Dumbrava","Nicoara","Nicoara"]

#a
print(colegi_de_clasa.sort())

#b
numar_aparitii={}

for i in range(len(colegi_de_clasa)):
    nr_aparitii_nume = numar_aparitii.get(colegi_de_clasa[i])
    if nr_aparitii_nume:        #daca exista adaugam 1 , daca nu exista ii punem 1 aparitie
        numar_aparitii[colegi_de_clasa[i]] = nr_aparitii_nume +1
    else:
        numar_aparitii[colegi_de_clasa[i]] = 1
print(numar_aparitii)


#c
numar_maxim = 0;
nume_cu_cele_mai_multe_aparitii = ""
for elem in numar_aparitii:
    if numar_aparitii[elem] > numar_maxim:
        numar_maxim = numar_aparitii[elem]
        nume_cu_cele_mai_multe_aparitii = elem

print(nume_cu_cele_mai_multe_aparitii + " a aparut de {0}".format(numar_maxim) )

#d
numar_minim = 100;
nume_cu_cele_mai_putine_aparitii = ""
for elem in numar_aparitii:
    if numar_aparitii[elem] < numar_minim:
        numar_minim = numar_aparitii[elem]
        nume_cu_cele_mai_putine_aparitii = elem

print(nume_cu_cele_mai_putine_aparitii + " a aparut de {0}".format(numar_minim) )


#e
print(colegi_de_clasa.sort(reverse= True))