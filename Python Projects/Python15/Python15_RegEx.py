from asyncio.windows_events import NULL
import re



# Expresii regulate
##########################
''' 
    Caractere regex:
        ^ = incepe cu 
        * = zero sau mai multe aparitii
        $ = se termina cu
        [] = set de caractere (ex.: [a-m])
        \ = semnalează o secvență specială (poate fi folosit și pentru a scăpa de caractere speciale)
            EX.:
                -> \s -	Un caracter spatiu (spatiu alb, tab, rand nou).
                -> \S -	Un caracter diferit de un spatiu.
                -> \d -	Un numar (intre 0-9)
                -> \D -	Un caracter ce nu este numar.
                -> \w -	Un caracter din intervalele a-z, A-Z, 0-9 si _
                -> \W -	Un caracter ce nu face parte din intervalele a-z, A-Z, 0-9 si _
        . = orice caracter (cu excepția caracterului newline) (ex.: "he..o)
        + = una sau mai multe apariții (rx.: "he.+o")
        ? = zero sau o apariție (ex.: "he.?o")
        {} = Exact numărul specificat de apariții (ex.: "he.{2}o")
        | = Fie sau (ex.: "cade|stă")
        () = Captură și grupare
'''


# Exemplu: Căutați intr-un text, pentru a vedea dacă începe cu „Ana” și se termină cu „mere”:
text = "Ana are mere"
x = re.search("^Ana.*mere$", text)  
if x:
    print("Da, textul contine Ana are mere!")
else:
    print("Nu contine!")


txt = "Mihai este afara. El vorbeste cu Bogdan."
# functia findall() returneaza o lista cu toate caracterele din expresia regex
x = re.findall("[a-m]", txt)    # ['i', 'h', 'a', 'i', 'e', 'e', 'a', 'f', 'a', 'a', 'l', 'b', 'e', 'e', 'c', 'g', 'd', 'a']
print(x)

txt1 = "Ioana a primit 4 cadouri cand si-a sarbatorit a 14 a aniversare."
x = re.findall("\d", txt1)      # ['4', '1', '4']
print(x)


# va returna o lista ce contine toate grupurile de litere "ar" intalnite:
txt2 = "Ariana are ardei"
x = re.findall("ar", txt2)      # ['ar', 'ar']
print(x)

# functia split() returnează o listă, în cazul în care șirul a fost impartit in functie de un caracter dat ca parametru. 
txt3 = "Ana are 6 mere si 5 portocale."
x = re.split("[0-9]", txt3)     # ['Ana are ', ' mere si ', ' portocale.']
print(x)

# Functia sub() inlocuieste toate caracterele care se potrivesc cu expresia data ca parametru, cu valoarea data de al doilea parametru.
txt4 = "Avem 2pere si 5 mere"
x = re.sub("[0-9]", "3", txt4)  # Avem 3pere si 3 mere
print(x)


# EXERCITIUL 1
print("EXERCITIUL 1:")
myText = "skdfjnkJWKDJNKm dkfjnKSJNDl KJDN edkjxnef "
if re.findall("[A-Z]", myText) != NULL:
    print("Exista litere mari")
else:
    print("Nu exista litere mari")

# EXERCITIUL 2
print("EXERCITIUL 2:")
myText2 = "skajnfleskjnalkejfn aeljnflaekjnf3298u42038 fdd3jd0"
print(re.split(" ", myText2))

# EXERCITIUL 3
print("EXERCITIUL 3:")
print(re.sub("[A-Z]", "*", myText2))

# EXERCITIUL 4
print("EXERCITIUL 4:")
myText3 = "kjs  ndljs\nlksd l\tlkmfls\'sldfk"
special = re.findall("\W", myText3)  
print(special)

# EXERCITIUL 5
myText4 = "Bunee si22cer senin555"
x = re.sub("eee|ee|555|5555|22", " ", myText4)
print(x)



##############################################################



# Functii Math

import math

# 1
notele = {
  "Vlad" : 9,
  "Alex" : 2,
  "Tudor" : 7,
  "Nicu" : 5
}

def CeaMaiMicaMedie(note):
    note_lista = []
    for key in note:
        note_lista.append(note[key])
    print(min(note_lista))

def CeaMaiMareMedie(note):
    note_lista = []
    for key in note:
        note_lista.append(note[key])
    print(max(note_lista))

CeaMaiMareMedie(notele)
CeaMaiMicaMedie(notele)


# 2
numere = [2.7, 3, -3]
numere_v = []
for elem in numere:
    if elem > math.floor(elem):
        numere_v.append(round(elem))
    elif elem < 0:
        numere_v.append(abs(elem))
    else:
        numere_v.append(elem)

print(sum(numere_v) / len(numere_v))


# 3
varsta_masina = []
Nume_Masini = []
for i in range(3):
    Nume_Masini.append(input("Introduceti un nume de masina: "))
    varsta_masina.append(int(input("Introduceti varsta masinii: ")))

print("Masinile au varstele adunate de : {0}".format(sum(varsta_masina)))
Masina_detalii = {}
for i in range(len(Nume_Masini)):
    Masina_detalii[Nume_Masini[i]] = varsta_masina[i]

print(Masina_detalii)



##############################################################



# Exceptii
# 1 & 2 
lista = []
nr_elemente = 5
print("Dati elementele listei: ")
for i in range(nr_elemente):
    try:
        x = int(input())
        if x == 0:
            raise Exception
    except ValueError:
        print("Introduceti doar intregi numere!")
    except Exception:
        print("Fara zero!")
        x = int(input())
        lista.append(x)
    else:
        lista.append(x)

print(lista)


# 3 & 4 & 5
n = 2 * len(lista)
try:
    for i in range(n):
        print(lista[i])
except IndexError:
    print("Ati depasit lungimea listei!")
else:
    print("Fara erori")
finally:
    print("Zona sensibila s-a terminat")



##############################################################



# JSON
import json

# 1
dict = {}
for i in range(5):
    cheie = input("Introdu o cheie")
    valoare = input("Introdu o valoare")
    dict[cheie] = valoare

# 2
y = json.dumps(dict)
print(y)

# 3
dict_indentat = json.dumps(dict, indent=2, separators=(',', ':') )
print(dict_indentat)

# 4
dict_normal = json.loads(dict_indentat)
print(dict_normal)
print(dict_normal["Amalia"])



##############################################################



# REGEX
# 1
text = "Ana are mere9, Maria are pere2, iar Ionel3 are prune5. Dan nu are nmk"
text_litere_mari = re.findall("[A-Z][A-Za-z]*", text)
print(text_litere_mari)


# 2
text_impartit = re.split(",|\.", text)
print(text_impartit)


#3
text_litere_mari_numere = re.findall("[^a-z-0-3\W]", text)
print(text_litere_mari_numere)


# 4
if(re.search("masina$", text)):
    print("se termina cu masina")
else:
    print("nu se termina cu masina")


# 5
print(text)
text = re.sub("er|ee|an| ", "#", text)



##############################################################



# Exercitii suplimentare
# EXERCITIUL 1
nr1 = int(20)
nr2 = float(10)
nume = "codeup"

restanta = 0
notaFinala = 10.0
laborator ="Programare Python"

print(type(restanta))
print(type(notaFinala))
print(type(laborator))


# ExERCITIUL 2
colegi_de_clasa = ["Istrate","Nicoara","Cerasela","Cristi","Filip","Nutu","Dumbrava","Istrate","Dumbrava","Nicoara","Nicoara"]

# a
print(colegi_de_clasa.sort())

# b
numar_aparitii = {}

for i in range(len(colegi_de_clasa)):
    nr_aparitii_nume = numar_aparitii.get(colegi_de_clasa[i])
    if nr_aparitii_nume:        #daca exista adaugam 1 , daca nu exista ii punem 1 aparitie
        numar_aparitii[colegi_de_clasa[i]] = nr_aparitii_nume +1
    else:
        numar_aparitii[colegi_de_clasa[i]] = 1
print(numar_aparitii)

# c
numar_maxim = 0
nume_cu_cele_mai_multe_aparitii = ""
for elem in numar_aparitii:
    if numar_aparitii[elem] > numar_maxim:
        numar_maxim = numar_aparitii[elem]
        nume_cu_cele_mai_multe_aparitii = elem

print(nume_cu_cele_mai_multe_aparitii + " a aparut de {0}".format(numar_maxim) )

# d
numar_minim = 100;
nume_cu_cele_mai_putine_aparitii = ""
for elem in numar_aparitii:
    if numar_aparitii[elem] < numar_minim:
        numar_minim = numar_aparitii[elem]
        nume_cu_cele_mai_putine_aparitii = elem

print(nume_cu_cele_mai_putine_aparitii + " a aparut de {0}".format(numar_minim) )

# e
print(colegi_de_clasa.sort(reverse= True))