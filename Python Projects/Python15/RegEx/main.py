import re
#1
text = "Ana are mere9, Maria are pere2, iar Ionel3 are prune5. Dan nu are nmk"

text_litere_mari = re.findall("[A-Z][A-Za-z]*", text)

print(text_litere_mari)
#2
text_impartit = re.split(",|\.", text)

print(text_impartit)
#3
text_litere_mari_numere = re.findall("[^a-z-0-3\W]",text)

print(text_litere_mari_numere)
#4
if(re.search("masina$",text)):
    print("se termina cu masina")
else:
    print("nu se termina cu masina")
#5
print(text)
text = re.sub("er|ee|an| ", "#",text)


print(text)