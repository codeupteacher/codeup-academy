import json

dict = {}

for i in range(5):
    cheie = input("Introdu o cheie")
    valoare = input("Introdu o valoare")
    dict[cheie] = valoare
	
y = json.dumps(dict)

print(y)

dict_indentat = json.dumps(dict, indent=2, separators=(',', ':') )

print(dict_indentat)

dict_normal = json.loads(dict_indentat)

print(dict_normal)

print(dict_normal["Amalia"])