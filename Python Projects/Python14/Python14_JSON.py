import json
import math

#CONVERTIREA din JSON in dictionar
# JSON-ul x:
x =  '{"name":"John", "age":30, "city":"New York"}'
# parsarea JSON-ului x cu ajutorul metodei loads():
y = json.loads(x)
print(y)
# afisarea din dictionarul rezultat mai sus
print(y["age"])


#CONVERTIREA tipurilor de date din Python in JSON
# avem un dictionar x:
x = {
 "name": "John",
 "age": 30,
 "city": "New York"
}
# convertirea dictionarului la JSON:
y = json.dumps(x)
# printarea JSON-ului obtinut
print(y)


#Tipurile de date care pot fi convertite
print(json.dumps({"name": "John", "age": 30}))      # dictionar
print(json.dumps(["apple", "bananas"]))             # lista
print(json.dumps(("apple", "bananas")))             # tuplu
print(json.dumps("hello"))                          # string
print(json.dumps(42))                               # int
print(json.dumps(31.76))                            # float
print(json.dumps(True))                             # boolean
print(json.dumps(False))
print(json.dumps(None))                             # none

#EXEMPLU + metoda dumps()
x = {
  "name": "Andrei",
  "age": 30,
  "married": True,
  "divorced": False,
  "children": ("Ana","Marian"),
  "pets": None,
  "cars": [
    {"model": "BMW 230", "mpg": 27.5},
    {"model": "Ford Edge", "mpg": 24.1}
  ]
}
print(json.dumps(x))
# indent=4 va crea 4 spatii
print(json.dumps(x, indent = 4))
# adaugarea parametrul  separators=(" . ", " = ") se va face pentru a schimba separatorul implicit
print(json.dumps(x, indent=4, separators=(" . ", " = ")))
# sort_keys = true va sorta alfabetic JSON-ul in functie de cheie (numele atributului)
print(json.dumps(x, indent = 4, sort_keys = True))


#EXERCITIUL 1
print("EXERCITIUL 1:")
dictionar = {"nume" : ("nume", "prenume"), "varsta": None, "inalt": True, "studii": {"liceu": "amintiri", "facultate": "WFH"}}

myJson = json.dumps(dictionar)
print(myJson)

jsonIndent = json.dumps(dictionar, indent = 3)
print(jsonIndent)

jsonSeparators = json.dumps(dictionar, separators = (" - ", " + "))
print(jsonSeparators)

jsonSort = json.dumps(dictionar, sort_keys = True)
print(jsonSort)


#EXERCITIUL 2
print("EXERCITIUL 2:")
a = [1, 2.7, 3]
b = ["nume", "prenume"]
c = [65,345,135,45,34,63,43,35,653,65]

print("SUBPUNCTUL a:")
print(f"a= {a}")
print(f"b= {b}")
print(f"c= {c}")

print("SUBPUNCTUL b+c:")

dict_cu_3_liste = { "a" : a, "b" : b, "c" : c}
myJson1 = json.dumps(dict_cu_3_liste)

print(myJson1)

print("SUBPUNCTUL d:")
print(dict_cu_3_liste["a"])

print("SUBPUNCTUL e:")
sum = 0
for i in dict_cu_3_liste["a"]:
    if type(i) == int or type(i) == float:
        sum += i
print(sum)

print("SUBPUNCTUL f:")
maxim = max(dict_cu_3_liste["c"])
print(maxim)


# EXERCITIUL 4
print("EXERCITIUL 4:")
listaMancarePreferata = ["pizza", "spaghetti", "lasagna", "friptura"]
myJson2 = json.dumps({"listaMancarePreferata" : listaMancarePreferata}, indent = 2, separators=(" : ", " -> "))
print(myJson2)


#TEMA
print("Tema :")
spiderman = {"Nume":"Spiderman","superputere":"Panza de paianjan","Varsta":25}
batman = {"Nume":"Batman","superputere":"Nu are","Varsta":35}
superman = {"Nume":"Superman","superputere":"Zboara","Varsta":40}

spidermanJson = json.dumps(spiderman,indent=2)
batmanJson = json.dumps(batman,indent=2,separators=("^","-"))
supermanJson = json.dumps(superman,indent=2,separators=("^","-"),sort_keys=True)

spiderman_reconvertit = json.loads(spidermanJson)
print(spiderman_reconvertit["Nume"])

batman_reconvertit = json.loads(batmanJson)
superman_reconvertit = json.loads(supermanJson)

superputeri = []
superputeri.append(spiderman_reconvertit["superputere"])
superputeri.append(batman["superputere"])
superputeri.append(superman["superputere"])

print(superputeri)


#TEMA Bonus
print("Tema Bonus:")

my_json = '{"text": "Nu avem numere negative", "verificator": false, "lista": [3, 5, 2, 25]}'
my_dict = json.loads(my_json)

text = my_dict["text"]
verificator = my_dict["verificator"]
lista = my_dict["lista"]

if(verificator):
        for elem in lista:
                if elem < 0:
                        print(elem)
else:
        print(text)