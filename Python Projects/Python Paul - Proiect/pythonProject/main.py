import tkinter as tk
root=tk.Tk()

root.geometry("600x600")

frame_lista = tk.Frame(root,bg = "white")
frame_lista.place(relwidth = 0.3,relheight =0.7,relx= 0.1,rely = 0.1)

frame_detalii = tk.Frame(root,bg = "white")
frame_detalii.place(relwidth = 0.4,relheight = 0.7,relx=0.5,rely = 0.1)


frame_butoane = tk.Frame(root,bg = "white")
frame_butoane.place(relwidth = 0.8 ,relheight = 0.1,relx=0.1,rely = 0.85)

label_titlu_lista = tk.Label(frame_lista, text="Persoane ",font = 30 ,bg='white', fg ='black')
label_titlu_lista.place(relx = 0.035,rely=0.05,relwidth =0.2,relheight=0.05)
label_titlu_lista.pack()


listbox = tk.Listbox(frame_lista,bg="yellow",selectmode = "BROWSE")
listbox.place(relx = 0.05,rely=0.1,relwidth =0.9,relheight=0.8)


lista_persoane = [["Popescu","Ion",14,"Pitesti"],
                  ["Ionescu","Vasile",17,"Bucuresti"],
                  ["Duta","Marius",11,"Ploiesti"],
                  ["Dumitru","Maria",12,"Constanta"]]


for i in range(len(lista_persoane)):
    listbox.insert(i,lista_persoane[i][0] + " " +lista_persoane[i][1])


root.mainloop()