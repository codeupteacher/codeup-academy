# import random module
import random
import time
def ChoiseIntToString(choiceInt):
    if choiceInt == 1:
        return 'Piatra'
    elif choiceInt == 2:
        return 'Hartie'
    else:
        return 'Foarfece'


def Definire_Castigator(choice, comp_choice,comp_choice_name):
    result = ""
    if ((choice == 1 and comp_choice == 2) or (choice == 2 and comp_choice == 1)):
        print("Hartie a invins! = > ", end="")
        result = "Hartie"
    elif ((choice == 1 and comp_choice == 3) or (choice == 3 and comp_choice == 1)):
        print("Piatra a invins! = > ", end="")
        result = "Piatra"
    elif ((choice == 2 and comp_choice == 3) or (choice == 3 and comp_choice == 2)):
        print("Foarfeca a invins! = > ", end="")
        result = "Foarfeca"
    elif (choice == comp_choice):
        print("Este egalitate. Reincepem runda")
        time.sleep(1)
        return False

    if result == comp_choice_name:
        print("Calculatorul a castigat")
    else:
        print(nume + " a castigat")
        time.sleep(1)

def salutareUtilizator():
    print("Hai sa ne jucam piatra-foarfeca-hartie.")
    time.sleep(1)
    nume = input("Cum te cheama? ")
    time.sleep(1)
    print("Salut " + nume + " regulile pentru castigarea jocului Piatra-Hartie-Foarfece sunt urmatoarele:\n"
+"Piatra vs Hartie->Hartie castiga\n"
+ "Piatra vs Foarfece ->Piatra castiga\n"
+"Hartie vs Foarfece ->Foarfecele castiga\n")
    return nume
nume = salutareUtilizator()
while True:
    print("""Introduceti
    alegerea 1,2 sau 3: \n
    1. Piatra\n
    2. Hartie\n
    3. Foarfece \n""")

    # take the input from user
    choice = int(input("Randul tau: "))

    # se va repeta pana cand se introduce o valoare valida
    while choice > 3 or choice < 1:
        choice = int(input("Introdu  o valoare valida. Adica 1 pentru piatra , 2 pentru hartie sau 3 pentru Foarfece "))

    choice_name =ChoiseIntToString(choice)
    print("Ai ales " + choice_name)

    time.sleep(1)

    print("\nAcum este randul calculatorului sa aleaga…….")

    time.sleep(1)
    comp_choice = random.randint(1, 3)
    comp_choice_name =ChoiseIntToString(comp_choice)
    print("Calculatorul a ales " + comp_choice_name)

    print(choice_name + " Vs " + comp_choice_name)

    time.sleep(1)
    # conditie pentru castigare
    AvemCastigator = Definire_Castigator(choice,comp_choice,comp_choice_name)
    if(AvemCastigator == False):
        continue

    ans = input("Mai vrei sa te joci? Selecteaza N daca vrei sa te opresti")
    if ans == 'n' or ans == 'N':
        break
    else:
        print("Urmeaza urmatoare runda")
        time.sleep(2)

print("Multumim ca te-ai jucat")