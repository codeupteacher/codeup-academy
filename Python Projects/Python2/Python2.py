# Exercitii
# 1. Scrieți un program Python pentru a crea tabelul de înmulțire (de la 1 la 10) al unui număr introdus de la tastatura.
n = int(input("Numarul introdus: "))
print(f"Tabla inmultirii pentru numarul ales, {n}:")
for i in range (1, 11):
    print(f"{n} x {i} = {n * i}")

# 2. Scrieti un program care afiseaza toate numerele pare de la 0 la x, unde x este un numar introdus de la tastatura.
x = int(input("Introduceti un numar:"))
for i in range(0, x + 1, 2):
    print(i)



# Tema
# 1. Afisati toate numelerele pare din intervalul -20, 20.
for i in range(-20, 21, 2):
    print(i)

# 2. Asemanator exercitiului de mai sus cu tabla inmultirii, afisati tabla impartirii pentru un anumit numar introdus.
n = int(input("Numarul introdus: "))
print(f"Tabla impartirii pentru numarul ales, {n}:")
for i in range (1, 11):
    print(f"{n} : {i} = {n / i}")

# 3. Scrieti un program care numara de la 1 la 50, dar sare peste numerele divizibile cu 10. (a se folosi continue)
for i in range(1, 51):
    if i % 10 == 0:
        continue
    print(i)

# 4. Scrieti un program care numara de la 1 la 50, dar care se va opri atunci cand/ daca ajunge la un numar x, introdus de la tastatura. (a se folosi break)
x = int(input("Introduceti un numar:"))
for i in range(-1000, 1000):
    if i == x:
        print(f"Am gasit numarul {i}")
        break