# 1   exercitiu
def divide(a, b):
    div = a / b
    try :
        b = 0
        return a / b
    except :
        print("Impartitorul trebuie sa fie diferit de 0")
    return div

a = int(input("a = "))
b = int(input("b = "))
print(divide(a, b))

# 2
prenume = "marinel"
try :
    prenume = prenume + 5
except :
    print("Nu poti concatena un int la string")

# 3
def readLista():
    length = int(input("Numarul de elemente din lista"))
    for i in range (length) :
        element = int(input())
        if element % 2 != 0:
            raise Exception("Lista nu poate contine numere impare")
        lista.append(element)
    return lista

lista = []
lista = readLista()
print(lista)
#4
def readLista1():
    length = int(input("Numarul de elemente din lista"))
    for i in range (length) :
        element = int(input())
        try :
            if element % 2 != 0:
                raise Exception("Lista nu poate contine numere impare")
        except Exception as e:
            print(e)
            element += 1
        else :
            element /= 2
        lista.append(element)
    return lista

lista1 = []
lista1 = readLista1()
print(lista1)
#5
def readLista2(lista):
    length = int(input("Numarul de elemente din lista"))
    for i in range (length) :
        element = int(input())
        try :
            if element % 2 != 0:
                raise Exception("Lista nu poate contine numere impare")
        except Exception as e:
            print(e)
            element += 1
        else :
            element /= 2
        finally :
            if element > 10 :
                print("nr este mai mare ca 10")
        lista.append(element)
    return lista

lista2 = []
lista2 = readLista2(lista2)
print(lista2)