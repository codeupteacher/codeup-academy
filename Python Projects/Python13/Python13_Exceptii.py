#Exemplu try-except-finally
try :
    f = open("blabla.txt")
    print(f)
except FileNotFoundError as err:
    print(err)
    print("Fisierul nu exista")

try :
    f = open("blabla.txt")
    print(f)
except FileNotFoundError:
    print("Fisierul nu exista")
finally :
    print("Hello")


# 1
def divide(a, b):
    try:
        return a / b
    except :
        return "Impartitorul trebuie sa fie diferit de 0"

a = int(input("a = "))
b = int(input("b = "))
print(divide(a, b))

# 2
prenume = "marinel"
try :
    prenume = prenume + 5
except :
    print("Nu poti concatena un int la string")

# 3
def readLista():
    lista = []
    length = int(input("Numarul de elemente din lista"))
    for i in range (length) :
        element = int(input())
        if element % 2 != 0:
            raise Exception("Lista nu poate contine numere impare")
        lista.append(element)
    return lista

lista = readLista()
print(lista)

# 4
def readLista1():
    lista = []
    length = int(input("Numarul de elemente din lista"))
    for i in range (length) :
        element = int(input())
        try :
            if element % 2 != 0:
                raise Exception("Lista nu poate contine numere impare")
        except Exception as e:
            print(e)
            element += 1
        else:
            element /= 2
        lista.append(element)
    return lista

lista1 = readLista1()
print(lista1)

# 5
def readLista2():
    lista = []
    length = int(input("Numarul de elemente din lista"))
    for i in range (length) :
        element = int(input())
        try :
            if element % 2 != 0:
                raise Exception("Lista nu poate contine numere impare")
        except Exception as e:
            print(e)
            element += 1
        else :
            element /= 2
        finally :
            if element > 10 :
                print("nr este mai mare ca 10")
        lista.append(element)
    return lista

lista2 = readLista2()
print(lista2)

# Exception -> prinde toate exceptiile
# as e -> in variabila e vom primi informatii despre exceptie


