# importam libraria time
import random
import time
import os
import colorama

lista = ["masina", "cuvinte", "bloc", "scaun"]
# saluta utilizatorul
print(colorama.Back.RED)
print(colorama.Style.BRIGHT + colorama.Style.BRIGHT + "asdasd")

def salutare():
    name = input(colorama.Fore.YELLOW + "What is your name?")
    print("Hello, " + name, "Time to play hangman!")
    print("")
    time.sleep(1)
    print("Start guessing…")
    time.sleep(0.5)
salutare()

def desenare_spanzuratoare(turns):
    if turns == 4:
        print(colorama.Fore.GREEN + """
___
|     |
|    
|  
|
|   
   """)
    if turns == 3:
        print(colorama.Fore.GREEN + """
___
|     |
|     O
|  
|
|   
   """)
    if turns == 2:
        print(colorama.Fore.GREEN + """
___
|     |
|     O
|    /|\ 
|
|   
   """)
    if turns == 1:
        print(colorama.Fore.GREEN + """
___
|     |
|     O
|    /|\ 
|     |
|   
   """)
    if turns == 0:
        print("""
    ___
    |     |
    |     O
    |    /|\ 
    |     |
    |    / \ 
    
       """)
# declara cuvantul care trebuie ghicit
secretword = random.choice(lista)

# creaza o variabila care salveaza incercarile userului
guesses = ""

# numarul de incimport coloramaercari
turns = 4

while turns > 0:
    afisare_frumoasa = ""
    litere_ramase_de_ghicit = 0
    guess = input(colorama.Fore.GREEN + "Introdu o litera:")
    os.system("cls")
    if len(guess) > 1:
        print(colorama.Fore.RED + "You have entered more than one character")
        continue
    guesses += guess
    for char in secretword:
        if char in guesses:
            afisare_frumoasa = afisare_frumoasa + char
        else:
            afisare_frumoasa = afisare_frumoasa + "*"
            litere_ramase_de_ghicit += 1
    print(afisare_frumoasa)
    print()
    if litere_ramase_de_ghicit == 0:
        print()
        print("You won!")
    if guess not in secretword:
        print()
        print(colorama.Fore.RED + "This letter is not in the word")
        turns -= 1
        print(colorama.Fore.RED + "YOU LOST!")
    desenare_spanzuratoare(turns)
    print(colorama.Fore.YELLOW + "You have {0} turns left".format(turns))


