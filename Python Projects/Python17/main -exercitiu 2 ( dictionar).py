import io
import json

dictionar = {"varsta":21,"nationalitate":"roman","pasiune":"citit","loc de munca":"CodeUp"}
y = json.dumps(dictionar)

try:
    fisier = open("json.txt","rt")
except FileNotFoundError:
    print("Fisierul nu exista")
except FileExistsError:
    print("Fisierul exista deja")
except io.UnsupportedOperation:
    print("Fisier deschis in format non-ok")
except:
    print("A aparut o eroare necunoscuta")
finally:
    print(y)
    fisier.close()
