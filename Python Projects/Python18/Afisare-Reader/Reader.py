import re
import  Afisare

def calculare_cel_mai_lung_cuvant(cuvinte) :
    max = 0
    cuvant_lung = ""
    for i in range(len(cuvinte)):
        if(max < len(cuvinte[i])):
            max = len(cuvinte[i])
            cuvant_lung = cuvinte[i]
    Afisare.print_cuvant_maxim(max , cuvant_lung)

def citire_cuvite_fisiere():
    try:
        fisier_citit = open("cuvinte.txt","r")
    except:
        open("cuvinte.txt","x")
        fisier_citit = open("cuvinte.txt","r")
    finally:
        text_citit = fisier_citit.read()
        cuvinte = re.split("\n",text_citit)
    calculare_cel_mai_lung_cuvant(cuvinte)

citire_cuvite_fisiere()

import collections
def citire_cuvite_fisiere_counters():
    try:
        fisier_citit = open("cuvinte.txt","r")
    except:
        open("cuvinte.txt","x")
        fisier_citit = open("cuvinte.txt","r")
    finally:
        text_citit = fisier_citit.read()
        cuvinte = re.split("\n",text_citit)
    nr_cuvinte1 = len(cuvinte)
    nr_cuvinte2 = collections.Counter(cuvinte)
    Afisare.print_numar_cuvinte(nr_cuvinte1, nr_cuvinte2)

citire_cuvite_fisiere_counters()

