'''
  Libraria curses creaza o fereastra independenta in terminalul python si ne ofera anumite facilitati de formatare si editare a ferestrei.
'''
import curses
import random

window_width = 60
window_height = 20

curses.initscr()
win = curses.newwin(window_height, window_width, 0, 0)  # cei doi de 0 sunt coordonatele x si y unde se plaseaza fereastra (stanga sus)
win.keypad(1)       # permite utilizatorului sa influenteze fereastra cu ajutorul sagetilor
curses.noecho()     # permite utilizatorului sa introduca o informatie fara sa fie nevoit sa dea "enter"
curses.curs_set(0)  # seteaza afisarea cursorului sau nu in functie de valoarea parametrului (True or False)
win.border(0, 0, 0, 0)  # deseneaza bordura default a jocului. Cei 4 parametri sunt: stanga, dreapta, sus, jos. Ex: win.border('|', '-', 'X', '+')
win.nodelay(1)      # precizeaza window-ului sa porneasca fara sa aiba delay (True or False) 

score = 0
snake = [(4, 4), (4, 3), (4, 2)]
food = (6, 6)
win.addch(food[0], food[1], "#")  # add character - adauga caracterul la coordonatele specificate
ESC = 27  # retine daca se apasa pe ESC
key = curses.KEY_RIGHT  # retine directia sarpelui, initial spre dreapta

while key != ESC:
  win.addstr(0, 2, " Score " + str(score) + " ")
  win.timeout(150 - (len(snake)) + len(snake))  # creste viteza in functie de lungimea sarpelui
  prev_key = key      # directia precedenta
  event = win.getch() # aduce ultima tasta apasata
  key = event if event != -1 else prev_key
  if key not in [curses.KEY_LEFT, curses.KEY_RIGHT, curses.KEY_UP, curses.KEY_DOWN, ESC]:
    key = prev_key 
  # Calcularea urmatoarei pozitii a sarpelui
  y = snake[0][0] # pozitia initiala a sarpelui in fereastra
  x = snake[0][1]
  if key == curses.KEY_DOWN:  # modificare pozitiei in functie de tasta apasata
    y += 1
  if key == curses.KEY_UP:
    y -= 1
  if key == curses.KEY_LEFT:
    x -= 1
  if key == curses.KEY_RIGHT:
    x += 1

  snake.insert(0, (y, x)) # insereaza un element la pozitia specificata 

  # Verificam daca atingem marginile
  if y == 0: break
  if y == window_height -1: break
  if x == 0: break
  if x == window_width -1: break
  # Verificam daca atingem propria coada
  if snake[0] in snake[1:]: break

  # Cresterea sarpelui daca mananca ceva
  if snake[0] == food:
    score += 1
    food = ()
    while food == ():
      food = (random.randint(1, window_height - 2), random.randint(1, window_width - 2))
      if food in snake:
        food = ()
    win.addch(food[0], food[1], "#")
  else:
    last = snake.pop()
    win.addch(last[0], last[1], " ")
  win.addch(snake[0][0], snake[0][1], "*")

curses.endwin()
print(f"Final score = {score}")