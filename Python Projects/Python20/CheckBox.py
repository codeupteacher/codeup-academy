from msilib.schema import CheckBox
from tabnanny import check
from tkinter import *

root = Tk()
root.geometry("500x500")
root.title("Q&A")

# TextBox + CheckBox
def check_answer(answer):
    INPUT = InputTxt.get("1.0", "end-1c")
    print(INPUT)
    if (INPUT == answer):
        Output.insert(END, 'Correct\n')
        InputTxt.delete("1.0", END)
    else:
        Output.insert(END, "Wrong answer\n")
        InputTxt.delete("1.0", END)

def Take_input():
    if var.get() == 0:
        Output.insert(END, "Nu poti verifica ecuatia\n")
        InputTxt.delete("1.0", END)
    elif var1.get() == 1:
        check_answer("2")
    elif var2.get() == 1:
        check_answer("-5")
    elif var3.get() == 1:
        check_answer("45")
    elif var4.get() == 1:
        check_answer("250")
    else:
        check_answer("500")

var = IntVar()
var1 = IntVar()
var2 = IntVar()
var3 = IntVar()
var4 = IntVar()

question = Label(root, text="Cat face 20*25?") 
InputTxt = Text(root, height=10, width=25, bg="light yellow")
checkbox = Checkbutton(root, text="Check Permission", variable=var, onvalue=1, offvalue=0)
Display = Button(root, height=2, width=20, text="Show", command= Take_input)
Output = Text(root, height=5, width=25, bg="light cyan")

checkbox1 = Checkbutton(root, text="Cat face 20/10?", variable=var1, onvalue=1, offvalue=0)
checkbox2 = Checkbutton(root, text="Cat face 20-25?", variable=var2, onvalue=1, offvalue=0)
checkbox3 = Checkbutton(root, text="Cat face 20+25?", variable=var3, onvalue=1, offvalue=0)
checkbox4 = Checkbutton(root, text="Cat face 10*25?", variable=var4, onvalue=1, offvalue=0)

question.pack()
InputTxt.pack()
checkbox.pack()
Display.pack()
Output.pack()

checkbox1.pack()
checkbox2.pack()
checkbox3.pack()
checkbox4.pack()

root.mainloop()