-- ONE TO ONE

CREATE  DATABASE myDatabase;

CREATE TABLE Students(
Person_Id INT  PRIMARY KEY,
Name VARCHAR(255),
Class VARCHAR(255),
Age VARCHAR(255)
);

CREATE TABLE Addresses(
Adresses_Id INT,
Address VARCHAR(255),
Student_Id INT UNIQUE FOREIGN KEY REFERENCES Students(Person_Id)
);

INSERT INTO  Students VALUES (1,'John','First','5');
INSERT INTO Students VALUES (2,'Mary','Third','7');
INSERT INTO Students VALUES (3,'Mike','Second','6');
INSERT INTO Students VALUES (4,'Linda','Third','7');
GO

INSERT INTO Addresses VALUES (1001, 'Mumbai', 1);
INSERT INTO Addresses VALUES (1002, 'Delhi', 2);
INSERT INTO Addresses VALUES (1003, 'BBSR', 3);
INSERT INTO Addresses VALUES (1004, 'Hyderabad', 4);
GO

SELECT * FROM Students
SELECT * FROM Addresses


-- ONE TO MANY

create table student(
 Id int primary key,
 Name varchar(255),
 Class varchar(255),
 Age int
 );

 create table adresses(
 AdressId int primary key,
 Adress varchar(255),
 StudentId int UNIQUE,
 FOREIGN KEY (StudentId) REFERENCES student(Id)
 );
 
 select * from adresses
 select * from student

 INSERT INTO student
VALUES (1,'Marius','a zecea',17),(2,'Alina','a opta',14),(3,'Andrei','a cincea',11),(4,'Bianca','a saptea',15);

 INSERT INTO adresses
VALUES (1002,'Prundu',2),(1003,'Trivale',3),(1004,'Tudor',4);







 create table employee(
 Id int primary key,
 Name varchar(255),
 Department varchar(255),
 Salary int,
 Gender varchar(255),
 Age int,
 City varchar(255),
);
create table projects(
 ProjectId int primary key,
 Title varchar(255),
 ClientId int,
 EmployeeId int,
 FOREIGN KEY (EmployeeId) REFERENCES employee(Id)
 );

  select * from employee
 select * from projects
  INSERT INTO employee
VALUES (1,'Gheorghe','IT',3500,'male',25,'Bucuresti'),(2,'Marian','Finance',2500,'male',27,'Pitesti'),
(3,'Alina','IT',5000,'female',30,'Ploiesti'),(4,'Gheorghe','Hr',3500,'male',25,'Bucuresti')

insert into projects
values (1,'development scratch',1,1),(2,'development python',2,1),(3,'',3,2),
(4,'MySQL DataBase',4,4),(5,'Company servers',5,1),(6,'WordPress website',6,3),(7,'development scratch',7,3);

-- CLASE- PROFESORI - ELEVI

create table profesori(
ProfesorId int primary key,
nume varchar(255),
varsta int,
materie varchar(255)
);
create table clase(
ClasaId int primary key,
ClasaNumar int,
ClasaLitera varchar(255),
profesorId int,
foreign key(profesorId) references profesori(ProfesorId)
);
create table elevi(
ElevId int primary key,
nume varchar(255),
varsta int,
clasaId int unique,
foreign key(clasaId) references clase(ClasaId)
);
 INSERT INTO profesori
VALUES(1,'Ionela',44,'romana'),(2,'Vlasceanu',38,'romana'),(3,'Vasile',30,'istorie'),(4,'Adrian',50,'chimie'),(5,'Tanasie',40,'matematica');

 INSERT INTO clase
VALUES(1,8,'C',1),(2,10,'A',2),(3,6,'B',3),(4,6,'A',4),(5,9,'F',5)