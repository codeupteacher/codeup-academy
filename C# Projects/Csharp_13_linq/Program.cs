﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Csharp_13_linq
{
    class Program
    {
        static void Main(string[] args)
        {
            //Exercitiul 1
            int[] lista = { -5, 7, 10, 22, 4, 6, 11 };
            Console.WriteLine(" Ex 1: input");
            foreach (int nr in lista)
                Console.Write("{0} ", nr);
            Console.WriteLine(" ");

            var numere_intre_1_si_11 = lista.Where(nr => nr > 1 && nr < 11).ToList<int>();
            Console.WriteLine(" Ex 1: output");
            foreach (int nr in numere_intre_1_si_11)
                Console.Write("{0} ", nr);

            //Exercitiul 2
            int[] lista2 = { 3, 5, 11, 5, 7, 3, 2, 2, 7 };
            Console.WriteLine(" Ex 2: input");
            foreach (int nr in lista2)
                Console.Write("{0} ", nr);
            Console.WriteLine(" ");
            var lista_de_frecvente = from nr in lista2
                                     group nr by nr;
            Console.WriteLine(" Ex 2: output");
            foreach (var nr in lista_de_frecvente)
            {
                Console.WriteLine("Numarul " + nr.Key + " apare de " + nr.Count());
            }

            //Exercitiul 3
            int[] lista3 = { 5, 1, 5, 2, 3, 5, 4, 5, 6, 8, 5, 6, 6, 4, 5, 2 };
            Console.WriteLine(" Ex 3: input");
            foreach (int nr in lista3)
                Console.Write("{0} ", nr);
            Console.WriteLine(" ");
            var lista_matrice = from nr in lista3
                                group nr by nr;
            Console.WriteLine(" Ex 3: output");
            Console.WriteLine("Numar " + " numar*frecventa " + " frecventa");
            foreach (var nr in lista_de_frecvente)
            {
                Console.WriteLine(nr.Key + "            " + nr.Key * nr.Count() + "               " + nr.Count());
            }

            //Exercitiul 4
            int[] lista4 = { 55, 200, 740, 76, 230, 482, 95 };
            Console.WriteLine(" Ex 4: input");
            foreach (int nr in lista4)
                Console.Write(nr + " ");
            Console.WriteLine(" ");
            var lista_nr_mari80 = from nr in lista4
                                  where nr > 80
                                  select nr;
            Console.WriteLine(" Ex 4: output");
            foreach (int nr in lista_nr_mari80)
            {
                Console.Write(nr + " ");
            }

            //Exercitiul 5
            List<string> lista5 = new List<string> { "m", "n", "p", "p", "o", "q", "m", "o", "p", "q", };
            Console.WriteLine(" ");
            Console.WriteLine(" Ex 5: input");
            foreach (string litera in lista5)
                Console.Write(litera + " ");
            Console.WriteLine(" ");
            var lista_fara_p = lista5.RemoveAll(litera => litera == "p");
            Console.WriteLine(" Lista fara p");
            foreach (string litera in lista5)
                Console.Write(litera + ", ");

            //Exercitiul 6
            lista5.RemoveRange(1, 2);
            Console.WriteLine("\n" + " Exercitiul 6");
            foreach (string litera in lista5)
                Console.Write(litera + ", ");

            //Exercitiul 7
            Console.WriteLine(" ");
            Console.Write("Introduceti numărul de siruri pentru a stoca în lista: " + "\n");
            string nr1 = Console.ReadLine();
            List<string> lista_cuvinte = new List<string>();
            Console.Write("Introduceti fiecare element din lista: " + "\n");
            for (int i = 0; i < Convert.ToInt32(nr1); i++)
            {
                Console.WriteLine("Element[{0}]: ", i);
                string cuvant = Console.ReadLine();
                lista_cuvinte.Add(cuvant);
            }
            Console.WriteLine("Introduceti lungimea minimă a articolului pe care doriti sa-l gasiti: ");
            string nr2 = Console.ReadLine();
            Console.WriteLine("Elementele de minim 4 caractere sunt: ");
            List<string> list_with_4chars_minimum = lista_cuvinte.Select(x => x).Where(x => x.Length > 4).ToList<string>();
            foreach (string cuv in list_with_4chars_minimum)
            {
                Console.WriteLine(cuv);
            }

            //Exercitiul 8
            Console.WriteLine("Introduceti cateva tari: ");
            List<string> lista_tari = new List<string>();
            lista_tari.Add(Console.ReadLine());
            //List<string> lista_tari2 = lista_tari.OrderBy(tari => tari.Length).ThenBy().ToList;

            //Exercitiul 9


            //Alt exercitiu
            Console.WriteLine("\n" + " Alt exercitiu ");
            Dictionary<int, int> my_dict = new Dictionary<int, int>();
            foreach (int nr in lista3)
            {
                if (my_dict.ContainsKey(nr))
                {
                    my_dict[nr]++;
                }
                else
                {
                    my_dict[nr] = 1;
                }
            }
            foreach (KeyValuePair<int, int> kvp in my_dict)
            {
                Console.WriteLine("Key = {0}, Value = {1}", kvp.Key, kvp.Value);
            }

        }
    }

}
