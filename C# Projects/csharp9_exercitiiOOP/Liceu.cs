﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp9_exercitiiOOP
{
    enum specializari
    {
        mate_info,
        filologie,
        stiinte_ale_naturii
    }
    internal class Liceu : UnitateDeInvatamant
    {
        public string numeUnitateDeInvatamt { get; set; }
        public string Adresa { get; set; }
        public int NrClase { get; set; }
        public int NrProfesori { get; set; }
        public double Mediescoala { get; set; }
        public List<Elev> EleviInUnitateaDeInvatamant { get; set; }
        public Elev CelMaiBunElev { get; set; }

        public Liceu(string numeLiceu, string adresa, int nrClase, int nrProfesori)
        {
            this.numeUnitateDeInvatamt = numeLiceu;
            this.Adresa = adresa;
            this.NrClase = nrClase;
            this.NrProfesori = nrProfesori;
            this.EleviInUnitateaDeInvatamant = new List<Elev>();
            this.CelMaiBunElev = new Elev();
        }

        public void AdaugaOClasa()
        {
            this.NrClase = this.NrClase + 1;
        }

        public void AfiseazaInformatiiDespeUnitateaDeInvatamant()
        {
            Console.WriteLine($"Aceasta unitate de invatamant este un liceu cu numele {numeUnitateDeInvatamt}.\nEl se afla" +
                $"la adresa :{this.Adresa}. El contine {NrProfesori} profesori, iar media liceului" +
                $"este {Mediescoala} si contine urmatoarele specializari: ");
            foreach(var specializare in Enum.GetNames(typeof(specializari)))
            {
                Console.WriteLine(specializare);
            }    
        }

        public void ModificaNrDeProfesori(int nrProfesori)
        {
            this.NrProfesori = nrProfesori;
        }

        public void RefreshElevi(List<Elev> eleviInScoala)
        {
            EleviInUnitateaDeInvatamant.Clear();
            foreach (var elev in eleviInScoala)
                if (elev.varsta >= 15 && elev.varsta < 19)
                    EleviInUnitateaDeInvatamant.Add(elev);

            SeteazaCelMaiBunElev();
            CalculeazaMedieScoala();
        }

        public void SeteazaCelMaiBunElev()
        {
            foreach (var elev in EleviInUnitateaDeInvatamant)
            {
                if (CelMaiBunElev.medie < elev.medie)
                    CelMaiBunElev = elev;
            }
        }
        public string GetInformatiiDespreCelMaiBunElev()
        {
            if (!String.IsNullOrEmpty(CelMaiBunElev.nume))
            {
                return $"Cel mai bun elev din liceul { numeUnitateDeInvatamt } este {CelMaiBunElev.nume} " +
                    $"cu media {CelMaiBunElev.medie} fiind in clasa {CelMaiBunElev.clasa}";
            }
            else
                return $"Cel mai bun elev nu a fost inca determiant.";
        }

        public void CalculeazaMedieScoala()
        {
            double suma_note = 0;
            foreach(var elev in EleviInUnitateaDeInvatamant)
            {
                suma_note += elev.medie;
            }
            Mediescoala = suma_note / EleviInUnitateaDeInvatamant.Count;
        }
        public void AfisareCorigenti()
        {
            foreach (var elev in EleviInUnitateaDeInvatamant)
            {
                if (elev.medie < 5.00)
                    Console.WriteLine("Corigent: " + elev.AfisareFrumoasa());
            }
        }
    }
}
