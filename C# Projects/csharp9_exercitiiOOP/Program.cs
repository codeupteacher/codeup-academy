﻿using System;

namespace csharp9_exercitiiOOP
{
    class Program
    {
        static void Main(string[] args)
        {
            // Display the number of command line arguments.
            Liceu Zinca = new Liceu("Zinca","Centru", 23, 44);
            Zinca.AfiseazaInformatiiDespeUnitateaDeInvatamant();


            ScoalaGenerala Nr14 = new ScoalaGenerala("Scoala Nr14","Nord", 12, 13);

            Elev elevPopescuPaul = new Elev("Popescu", "Paul", 13, 7, 9);
            Elev elevIonescuCristi = new Elev("Ionescu", "Cristi", 14, 8, 4);
            Elev elevVasileMaria = new Elev("Vasile", "Maria", 15, 9, 6.8);
            Elev elevIonIoana = new Elev("Ion", "Ioana", 16, 10, 9.57);
            Elev elevVasileSilviu = new Elev("Vasile", "Silviu", 17, 11, 9.4);
            List<Elev> eleviToBeIntroduced = new List<Elev>();
            eleviToBeIntroduced.Add(elevPopescuPaul);
            eleviToBeIntroduced.Add(elevIonescuCristi);
            eleviToBeIntroduced.Add(elevVasileMaria);
            eleviToBeIntroduced.Add(elevIonIoana);
            eleviToBeIntroduced.Add(elevVasileSilviu);


            Nr14.RefreshElevi(eleviToBeIntroduced);
            Zinca.RefreshElevi(eleviToBeIntroduced);

            foreach (var elev in Nr14.EleviInUnitateaDeInvatamant)
                Console.WriteLine(elev.AfisareFrumoasa());
            Nr14.SeteazaCelMaiBunElev();
            Zinca.SeteazaCelMaiBunElev();

            Console.WriteLine(Nr14.GetInformatiiDespreCelMaiBunElev());
            Console.WriteLine(Zinca.GetInformatiiDespreCelMaiBunElev());
            Nr14.CalculeazaMedieScoala();
            Zinca.CalculeazaMedieScoala();

            if(Zinca.Mediescoala > Nr14.Mediescoala)
            {
                Console.WriteLine($"{Zinca.numeUnitateDeInvatamt} este mai buna decat {Nr14.numeUnitateDeInvatamt} " +
                    $"avand media de {Zinca.Mediescoala}, {Zinca.GetInformatiiDespreCelMaiBunElev()}");
            }
            else
            {

                Console.WriteLine($"{Nr14.numeUnitateDeInvatamt} este mai buna decat {Zinca.numeUnitateDeInvatamt} " +
                    $"avand media de {Nr14.Mediescoala}, {Nr14.GetInformatiiDespreCelMaiBunElev()}");
            }

            Zinca.AfisareCorigenti();
            Nr14.AfisareCorigenti();
        }
    }
}