﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp9_exercitiiOOP
{
    internal class Elev
    {
        public Elev()
        { }
        public Elev(string nume,string prenume, int varsta, int clasa, double medie)
        {
            this.nume = nume;
            this.prenume = prenume;
            this.varsta = varsta;
            this.clasa = clasa;
            this.medie = medie;
        }

        public specializari specializareamea { get; set; }
        public string nume{ get; set; }
        public string prenume{get;set;}
        public int varsta { get; set; } 
        public int clasa { get; set; }
        public double medie{ get; set; }
        public string AfisareFrumoasa()
        {
            return $"Informatii Elev:\n" +
                $"Nume {nume}" +
                $"Prenume {prenume}" +
                $"Varsta {varsta}" +
                $"Clasa {clasa}" +
                $"Media {medie}";
        }
    }
}
