﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp9_exercitiiOOP
{
    enum TipInvatamant
    {
        Prescolara,
        Clasele1_4,
        Gimnaziala,
        Liceala
    }
    internal interface UnitateDeInvatamant
    {

        List<Elev> EleviInUnitateaDeInvatamant { get; set; }
        string numeUnitateDeInvatamt { get; set; }
        string Adresa { get; set; }
        int NrClase { get; set; }
        int NrProfesori { get; set; }
        double Mediescoala { get; set; }
        public Elev CelMaiBunElev { get; set; }
        void AdaugaOClasa();
        void SeteazaCelMaiBunElev();
        string GetInformatiiDespreCelMaiBunElev();
        void ModificaNrDeProfesori(int nrProfesori);
        void AfiseazaInformatiiDespeUnitateaDeInvatamant();
        void RefreshElevi(List<Elev> eleviInScoala);
        void CalculeazaMedieScoala();
        void AfisareCorigenti();

    }
}
