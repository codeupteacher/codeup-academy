﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp8
{
    interface IFemeie
    {
        bool EsteFemeie { get; set; }
        string nume { get; set; }
        int varsta { get; set; }
        bool EsteParinte { get; set; }
        int numarCopii { get; set; }

        public string AfisareNumarCopii();
    }
}
