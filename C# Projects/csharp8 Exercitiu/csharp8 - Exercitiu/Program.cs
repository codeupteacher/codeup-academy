﻿using System;

namespace csharp8
{

    class Program
    {
        static void Main(string[] args)
        {
            LimbaRomana limbaRomana = new LimbaRomana();
            limbaRomana.ContineDiacritice = true;
            Console.WriteLine(limbaRomana.NumarDeDiacriticeDataExista());
            Console.WriteLine(limbaRomana.CeaMaiNeuitilizataLitera());
            Console.WriteLine(limbaRomana.CeaMaiUtilizataLitera());

            Om barbat = new Om("Ion", 24, true, false, 4);
            Console.WriteLine(barbat.AFacutArmata());
            Console.WriteLine(barbat.AfisareNumarCopii());
            Console.WriteLine(barbat.EsteMajor());

            Om femeie = new Om("Maria", 22, false, true, 4);
            Console.WriteLine(femeie.AFacutArmata());
            Console.WriteLine(femeie.AfisareNumarCopii());
            Console.WriteLine(femeie.EsteMajor());
        }
    }
}
