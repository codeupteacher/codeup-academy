﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp8
{
    abstract class LimbaVorbita
    {
        public abstract bool ContineDiacritice { get; set; }
        public abstract bool ContineLitere { get; set; }

        public abstract string CeaMaiUtilizataLitera();
        public abstract string CeaMaiNeuitilizataLitera();
        public string NumarDeDiacriticeDataExista()
        {
            if(ContineDiacritice)
            {
                return "Limba contine diacritice";
            }
            else
            {
                return "Limba NU contine diacritice";
            }
        }
    }
}
