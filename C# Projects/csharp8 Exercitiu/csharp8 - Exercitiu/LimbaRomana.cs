﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp8
{
    class LimbaRomana : LimbaVorbita
    {
        public override bool ContineDiacritice { get ; set ; }
        public override bool ContineLitere { get ; set ; }

        public override string CeaMaiNeuitilizataLitera()
        {
            return "a";
        }

        public override string CeaMaiUtilizataLitera()
        {
            return "x";
        }
    }
}
