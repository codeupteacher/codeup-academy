﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp8
{
    interface IBarbat
    {
        bool EsteBarbat { get; set; }
        string nume { get; set; }
        int varsta { get; set; }

        public string AFacutArmata();
        public string EsteMajor();

    }
}
