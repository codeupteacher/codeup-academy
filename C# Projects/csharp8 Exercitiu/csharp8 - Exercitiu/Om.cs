﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp8
{
    class Om : IBarbat, IFemeie
    {
        public string nume { get; set; }
        public int varsta { get; set ; }
        public bool EsteBarbat { get; set; }
        public bool EsteFemeie { get; set; }
        public bool EsteParinte { get; set; }
        public int numarCopii { get; set; }
        public Om(string nume ,int varsta, bool EsteParinte, bool EsteFemeie, int numarCopii)
        {
            this.nume = nume;
            this.varsta = varsta;
            if(EsteFemeie)
            {
                this.EsteFemeie = true;
            }
            else
            {
                this.EsteBarbat = true;
            }
            if (EsteParinte)
            {
                this.EsteParinte = true;
                this.numarCopii = numarCopii;
            }
            else
            {
                this.numarCopii = 0;
            }
        }
        public string AFacutArmata()
        {
            if (EsteBarbat && varsta > 20)
                return nume + " a facut armata"; 
            else
                return nume + " nu a facut armata";
        }

        public string EsteMajor()
        {
            if (varsta > 18)
                if(EsteBarbat)
                    return nume + "  este major";
                else
                    return nume + "  este majora";
            else
                if (EsteBarbat)
                    return nume + " NU este major";
                else
                    return nume + " NU este majora";
        }

        public string AfisareNumarCopii()
        {
            if (EsteParinte)
            {
                return nume + " are " + numarCopii + " copii frumosi.";
            }
            else
            {
                return nume + " nu are copii";
            }
        }
    }
}
