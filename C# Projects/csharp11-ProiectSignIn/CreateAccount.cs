﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace csharp11
{
    public partial class CreateAccount : Form
    {
        public CreateAccount()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Utilizator utilizator = new Utilizator();

            utilizator.Nume1 = TNume.Text;
            utilizator.Prenume1 = TPrenume.Text;
            utilizator.Varsta = int.Parse(TVarsta.Text);
            utilizator.Email = TEmail.Text;
            utilizator.Tel = long.Parse(TTelefon.Text);
            utilizator.Parola = TParola.Text;
            utilizator.Intrebare = CIntrebare.Text;
            utilizator.raspuns1 = TRaspuns.Text;
          
            AccountManager.ScrieDate(utilizator);
            MessageBox.Show("Utilizator introdus");
            this.Close();
        }
      
    }
}
