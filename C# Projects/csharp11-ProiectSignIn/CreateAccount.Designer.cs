﻿
namespace csharp11
{
    partial class CreateAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TNume = new System.Windows.Forms.TextBox();
            this.TPrenume = new System.Windows.Forms.TextBox();
            this.TVarsta = new System.Windows.Forms.TextBox();
            this.TParola = new System.Windows.Forms.TextBox();
            this.TTelefon = new System.Windows.Forms.TextBox();
            this.TEmail = new System.Windows.Forms.TextBox();
            this.btnCreareCont = new System.Windows.Forms.Button();
            this.LSecurityQ = new System.Windows.Forms.Label();
            this.CIntrebare = new System.Windows.Forms.ComboBox();
            this.TRaspuns = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(21, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(453, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pentru a crea un nou cont, completeaza urmatoarele informatii:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(294, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(224, 31);
            this.label2.TabIndex = 1;
            this.label2.Text = "CREARE CONT NOU";
            // 
            // TNume
            // 
            this.TNume.Location = new System.Drawing.Point(32, 112);
            this.TNume.Name = "TNume";
            this.TNume.PlaceholderText = "nume";
            this.TNume.Size = new System.Drawing.Size(271, 27);
            this.TNume.TabIndex = 2;
            // 
            // TPrenume
            // 
            this.TPrenume.Location = new System.Drawing.Point(388, 112);
            this.TPrenume.Name = "TPrenume";
            this.TPrenume.PlaceholderText = "prenume";
            this.TPrenume.Size = new System.Drawing.Size(274, 27);
            this.TPrenume.TabIndex = 3;
            // 
            // TVarsta
            // 
            this.TVarsta.Location = new System.Drawing.Point(32, 160);
            this.TVarsta.Name = "TVarsta";
            this.TVarsta.PlaceholderText = "varsta";
            this.TVarsta.Size = new System.Drawing.Size(271, 27);
            this.TVarsta.TabIndex = 4;
            // 
            // TParola
            // 
            this.TParola.Location = new System.Drawing.Point(388, 211);
            this.TParola.Name = "TParola";
            this.TParola.PasswordChar = '*';
            this.TParola.PlaceholderText = "parola";
            this.TParola.Size = new System.Drawing.Size(274, 27);
            this.TParola.TabIndex = 5;
            // 
            // TTelefon
            // 
            this.TTelefon.Location = new System.Drawing.Point(32, 211);
            this.TTelefon.Name = "TTelefon";
            this.TTelefon.PlaceholderText = "telefon";
            this.TTelefon.Size = new System.Drawing.Size(271, 27);
            this.TTelefon.TabIndex = 6;
            // 
            // TEmail
            // 
            this.TEmail.Location = new System.Drawing.Point(388, 160);
            this.TEmail.Name = "TEmail";
            this.TEmail.PlaceholderText = "email";
            this.TEmail.Size = new System.Drawing.Size(274, 27);
            this.TEmail.TabIndex = 7;
            // 
            // btnCreareCont
            // 
            this.btnCreareCont.Location = new System.Drawing.Point(390, 409);
            this.btnCreareCont.Name = "btnCreareCont";
            this.btnCreareCont.Size = new System.Drawing.Size(272, 29);
            this.btnCreareCont.TabIndex = 8;
            this.btnCreareCont.Text = "Creare Cont";
            this.btnCreareCont.UseVisualStyleBackColor = true;
            this.btnCreareCont.Click += new System.EventHandler(this.button1_Click);
            // 
            // LSecurityQ
            // 
            this.LSecurityQ.AutoSize = true;
            this.LSecurityQ.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.LSecurityQ.Location = new System.Drawing.Point(32, 309);
            this.LSecurityQ.Name = "LSecurityQ";
            this.LSecurityQ.Size = new System.Drawing.Size(223, 20);
            this.LSecurityQ.TabIndex = 9;
            this.LSecurityQ.Text = "Choose Your Security Question";
            // 
            // CIntrebare
            // 
            this.CIntrebare.FormattingEnabled = true;
            this.CIntrebare.Items.AddRange(new object[] {
            "Culoarea preferata?",
            "Numele animalului?",
            "Masina preferata?"});
            this.CIntrebare.Location = new System.Drawing.Point(32, 341);
            this.CIntrebare.Name = "CIntrebare";
            this.CIntrebare.Size = new System.Drawing.Size(283, 28);
            this.CIntrebare.TabIndex = 10;
            // 
            // TRaspuns
            // 
            this.TRaspuns.Location = new System.Drawing.Point(389, 341);
            this.TRaspuns.Name = "TRaspuns";
            this.TRaspuns.PlaceholderText = "raspuns";
            this.TRaspuns.Size = new System.Drawing.Size(267, 27);
            this.TRaspuns.TabIndex = 11;
            // 
            // CreateAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TRaspuns);
            this.Controls.Add(this.CIntrebare);
            this.Controls.Add(this.LSecurityQ);
            this.Controls.Add(this.btnCreareCont);
            this.Controls.Add(this.TEmail);
            this.Controls.Add(this.TTelefon);
            this.Controls.Add(this.TParola);
            this.Controls.Add(this.TVarsta);
            this.Controls.Add(this.TPrenume);
            this.Controls.Add(this.TNume);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "CreateAccount";
            this.Text = "CreateAccount";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TNume;
        private System.Windows.Forms.TextBox TPrenume;
        private System.Windows.Forms.TextBox TVarsta;
        private System.Windows.Forms.TextBox TParola;
        private System.Windows.Forms.TextBox TTelefon;
        private System.Windows.Forms.TextBox TEmail;
        private System.Windows.Forms.Button btnCreareCont;
        private System.Windows.Forms.Label LSecurityQ;
        private System.Windows.Forms.ComboBox CIntrebare;
        private System.Windows.Forms.TextBox TRaspuns;
    }
}