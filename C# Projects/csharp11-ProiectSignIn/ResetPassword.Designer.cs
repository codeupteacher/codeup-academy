﻿
namespace csharp11
{
    partial class ResetPassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtUserRec = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            this.labelReset = new System.Windows.Forms.Label();
            this.lblRec = new System.Windows.Forms.Label();
            this.lblrasp = new System.Windows.Forms.TextBox();
            this.verificaRecovery = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtUserRec
            // 
            this.txtUserRec.Location = new System.Drawing.Point(49, 75);
            this.txtUserRec.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtUserRec.Name = "txtUserRec";
            this.txtUserRec.PlaceholderText = "text";
            this.txtUserRec.Size = new System.Drawing.Size(155, 31);
            this.txtUserRec.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 46);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Username";
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(329, 70);
            this.btnNext.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(118, 36);
            this.btnNext.TabIndex = 2;
            this.btnNext.Text = "next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // labelReset
            // 
            this.labelReset.AutoSize = true;
            this.labelReset.Location = new System.Drawing.Point(49, 178);
            this.labelReset.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelReset.Name = "labelReset";
            this.labelReset.Size = new System.Drawing.Size(0, 25);
            this.labelReset.TabIndex = 3;
            // 
            // lblRec
            // 
            this.lblRec.AllowDrop = true;
            this.lblRec.AutoSize = true;
            this.lblRec.Location = new System.Drawing.Point(49, 326);
            this.lblRec.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRec.Name = "lblRec";
            this.lblRec.Size = new System.Drawing.Size(58, 25);
            this.lblRec.TabIndex = 4;
            this.lblRec.Text = "lblRec";
            this.lblRec.UseWaitCursor = true;
            this.lblRec.Click += new System.EventHandler(this.label2_Click);
            // 
            // lblrasp
            // 
            this.lblrasp.Location = new System.Drawing.Point(49, 394);
            this.lblrasp.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lblrasp.Name = "lblrasp";
            this.lblrasp.Size = new System.Drawing.Size(155, 31);
            this.lblrasp.TabIndex = 5;
            // 
            // verificaRecovery
            // 
            this.verificaRecovery.Location = new System.Drawing.Point(524, 390);
            this.verificaRecovery.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.verificaRecovery.Name = "verificaRecovery";
            this.verificaRecovery.Size = new System.Drawing.Size(118, 74);
            this.verificaRecovery.TabIndex = 6;
            this.verificaRecovery.Text = "Recover PAss";
            this.verificaRecovery.UseVisualStyleBackColor = true;
            this.verificaRecovery.Click += new System.EventHandler(this.verificaRecovery_Click);
            // 
            // ResetPassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 562);
            this.Controls.Add(this.verificaRecovery);
            this.Controls.Add(this.lblrasp);
            this.Controls.Add(this.lblRec);
            this.Controls.Add(this.labelReset);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtUserRec);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ResetPassword";
            this.Text = "ResetPassword";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtUserRec;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Label labelReset;
        private System.Windows.Forms.Label lblRec;
        private System.Windows.Forms.TextBox lblrasp;
        private System.Windows.Forms.Button verificaRecovery;
    }
}