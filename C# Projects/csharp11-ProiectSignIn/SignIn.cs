﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace csharp11
{
    public partial class SignIn : Form
    {
        public SignIn()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            String username = TxtUser.Text;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            String parola = txtPsw.Text;
        }

        private void btnSignIn_Click(object sender, EventArgs e)
        {
            string user = TxtUser.Text;
            string psw = txtPsw.Text;
            bool check = AccountManager.CheckSignUser(user,psw);

            if (!check)
            {
                MessageBox.Show("Date incorecte");
            }
            else
            {
                ProgramPrincipal pp1 = new ProgramPrincipal(); // Instantiatiem un obiect ProgramPrincipal
                pp1.Show(); // activam/afisam obiectul -- adica deschidem form ul Program Principal
            }


        }

        private void btnCreateAcc_Click(object sender, EventArgs e)
        {
            CreateAccount cr1 = new CreateAccount();
            cr1.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ResetPassword rp = new ResetPassword();
            rp.Show();
        }
    }
}
