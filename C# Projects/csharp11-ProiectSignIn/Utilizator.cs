﻿using System;
using System.Collections.Generic;
using System.Text;

namespace csharp11
{
    class Utilizator
    {
        String Nume;
        String Prenume;
        int varsta;
        String email;
        long tel;
        String parola;
        String intrebare;
        String raspuns;
        public Utilizator() {  }
        public Utilizator(string nume, string prenume, int varsta, string email, long tel, string parola, string intrebare, string raspuns)
        {
            Nume = nume;
            Prenume = prenume;
            this.varsta = varsta;
            this.email = email;
            this.tel = tel;
            this.parola = parola;
            this.intrebare = intrebare;
           
            this.raspuns = raspuns;
        }

        public string Nume1 { get => Nume; set => Nume = value; }
        public string Prenume1 { get => Prenume; set => Prenume = value; }
        public int Varsta { get => varsta; set => varsta = value; }
        public string Email { get => email; set => email = value; }
        public long Tel { get => tel; set => tel = value; }
        public string Parola { get => parola; set => parola = value; }
        public string Intrebare { get => intrebare; set => intrebare = value; }
     
        public string raspuns1 { get => raspuns; set => raspuns = value; }
    }
}
