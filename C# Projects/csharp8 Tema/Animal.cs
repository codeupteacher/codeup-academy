﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teme_csharp8
{
    abstract class Animal
    {
        public abstract string tipAnimal { get; set; }
        public abstract string varsta { get; set; }
        public abstract void Sunet();

        public void Doarme()
        {
            Console.WriteLine("zzzzzzz");
        }

    }
}
