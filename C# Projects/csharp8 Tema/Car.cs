﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teme_csharp8
{
    internal class Car : IVehicul
    {
        public string Marca { get ; set ; }
        public int anFabricatie { get; set; }
        public int nrLocuri { get; set ; }
        public string culoare { get; set ; }

        public bool EsteMasinaNoua()
        {
            if (anFabricatie > 2017)
                return true;
            else
                return false;
        }

        public void SchimbaCuloare(string culoarea)
        {
            this.culoare = culoare;
        }
    }
}
