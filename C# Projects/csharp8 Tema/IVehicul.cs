﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teme_csharp8
{
    internal interface IVehicul
    {
        public string Marca { get; set; }
        public int anFabricatie { get; set; }
        public int nrLocuri { get; set; }
        public string culoare { get; set; }

        public void SchimbaCuloare(string culoarea);
        public bool EsteMasinaNoua();
    }
}
