﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace csharp10
{
    public partial class PrimulMeuForm : Form
    {
        public PrimulMeuForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = "Am creat primul meu form EVER care activeaza acest text cand apas un buton";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label1.Text = "";
        }
    }
}
