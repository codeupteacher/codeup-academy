﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace csharp10
{
    public partial class AlTreileaForm : Form
    {
        public AlTreileaForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                label1.Text = "checkBox-ul este selectat";
            }
            else
            {
                label1.Text = "checkBox-ul nu este selectat";
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string textToBeShowed = "";
            foreach(var item in checkedListBox1.CheckedItems)
            {
                textToBeShowed += item.ToString() + " ";
            }

            if(textToBeShowed == "")
            {
                label2.Text = "Nu este selectat nimic";
            }
            else
            {
                label2.Text = "Sunt selectate urmatoarele checkbox-uri :" + textToBeShowed;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex >= 0)
            {
                label3.Text = "Este selectat " + comboBox1.Text;
            }
            else
            {
                label3.Text = "Nu este selectat nimic";
            }
        }
    }
}
