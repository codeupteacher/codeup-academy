
CREATE TABLE Artists (
    ArtistId int  PRIMARY KEY ,
    ArtistName varchar(255),
);

CREATE TABLE Genre (
    GenreId int  PRIMARY KEY ,
    GenreName varchar(255),
);
CREATE TABLE Albums (
    AlbumId int  PRIMARY KEY ,
    AlbumName varchar(255),
    ArtistId int  NOT NULL  ,
    GenreId int  NOT NULL  ,
	FOREIGN KEY (ArtistId) REFERENCES Artists(ArtistId),
	FOREIGN KEY (GenreId) REFERENCES Genre(GenreId)
);

select * from Albums
select * from Genre
select * from Artists

INSERT INTO Artists
VALUES (1, 'Connecter'),(2, 'AC/DC'),(3, 'Adi Minune')

INSERT INTO Genre
VALUES (1, 'Rap'),(2, 'rock'),(3, 'Muzica pentru toata lumea')

INSERT INTO Albums
VALUES (3, 'Album3',3,3),(2, 'Highway to hell',2,2),(1, 'Vara nu dorm',1,1)