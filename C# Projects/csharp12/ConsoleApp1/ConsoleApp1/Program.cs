﻿using System;
using System.Linq;

namespace ConsoleApp1
{
    class User
    {
        public string Nume;
        public string Prenume;
        public string Email;
        public string Password;
        public string Nickname;
        public int Age;
        public string Gender;

        public User(string Nume, string Prenume, string Email, string Password, string Nickname, int Age, string Gender)
        {
            this.Nume = Nume;
            this.Prenume = Prenume;
            this.Email = Email;
            this.Password = Password;
            this.Nickname = Nickname;
            this.Age = Age;
            this.Gender = Gender;
        }
    }

    class Program
    {
        static void ShowUsers(User[] userArray)
        {
            foreach (User u in userArray)
            {
                Console.WriteLine($"Prenume = {u.Prenume} | Nume = {u.Nume} | Email = {u.Email} | Age = {u.Age} | Parola = {u.Password}");
            }
        }

        static void NewLine()
        {
            Console.WriteLine("\n");
        }

        static void Main(string[] args)
        {
            User[] userArray =
            {
                new User("Bentia", "David", "davidhamaba@gmail.com", "david12", "Amanel", 13, "Barbat"),
                new User("Nitescu", "Stefan", "stefinitescu@gmail.com", "stef10", "woot", 14, "Barbat"),
                new User("Dragomir", "Cristi", "cristi2007dragomir@gmail.com", "frost123", "Frost", 19, "Barbat"),
                new User("Maqlan", "Amira", "maqlanamira@gmail.com", "maqla15", "Jalakis", 13, "Femeie"),
                new User("Bentia", "Anda", "bentiaanda@gmail.com", "anda123", "Anda", 20, "Femeie")
            };

            Console.WriteLine("Exercitiu Where");

            var __userArrayMajor = from u in userArray
                                   where u.Age > 18
                                   select u;

            var __userArrayMajorMale = from u in userArray
                                       where u.Age > 18 && u.Gender == "Barbat"
                                       select u;

            var userArrayMajor = userArray.Where(u => u.Age > 18).ToArray();

            var userArrayMajorMale = userArray.Where(u => u.Age > 18 && u.Gender == "Barbat").ToArray();





            Console.WriteLine("Exercitiu Sorting");

            var __userArrayMajorSorted = from u in userArray
                                         where u.Age > 18
                                         orderby u.Age ascending
                                         select u;

            var __userArrayMajorGenderSorted = from u in userArray
                                         where u.Age > 18
                                         orderby u.Gender ascending,  u.Age ascending
                                         select u;

            var userArrayMajorSorted = userArray.OrderByDescending(u => u.Age).Where(u => u.Age > 18);

            var userArrayMajorMaleSorted = userArray.OrderByDescending(u => u.Gender).OrderByDescending(u => u.Age).Where(u => u.Age > 18);


            Console.WriteLine("sintaxa clasica majori");

            ShowUsers(__userArrayMajor.ToArray());

            NewLine();


            Console.WriteLine("sintaxa clasica majori barbati");

            ShowUsers(__userArrayMajorMale.ToArray());

            NewLine();


            Console.WriteLine("where majori");

            ShowUsers(userArrayMajor.ToArray());

            NewLine();


            Console.WriteLine("where majori barbati");

            ShowUsers(userArrayMajorMale.ToArray());

            NewLine();

            Console.WriteLine("sintaxa clasica majori sorted crescator");

            ShowUsers(__userArrayMajorSorted.ToArray());

            NewLine();


            Console.WriteLine("sintaxa clasica majori barbati sorted crescator");

            ShowUsers(__userArrayMajorGenderSorted.ToArray());

            NewLine();


            Console.WriteLine("where majori sorted descrescator");

            ShowUsers(userArrayMajorSorted.ToArray());

            NewLine();


            Console.WriteLine("where majori barbati sorted descrescator");

            ShowUsers(userArrayMajorMaleSorted.ToArray());



            Console.WriteLine("Exercitiu GroupBy");

            var _groupedResult = from u in userArray
                                group u by u.Gender;

            var groupedResult = userArray.OrderByDescending(u => u.Age).GroupBy(u => u.Gender);

            foreach (var ageGroup in _groupedResult)
            {
                Console.WriteLine($"gender group: {ageGroup.Key}");

                foreach (User u in ageGroup)
                    Console.WriteLine($"name: {u.Nume} {u.Prenume}");
            }

            NewLine();

            foreach (var ageGroup in groupedResult)
            {
                Console.WriteLine($"gender group: {ageGroup.Key}");

                foreach (User u in ageGroup)
                    Console.WriteLine($"name: {u.Nume} {u.Prenume} | {u.Age}");
            }



            Console.WriteLine("Exercitiu Select");

            var _majorUsersEmail = from u in userArray
                                   where u.Age > 18
                                   select u.Email;

            var _majorUsersNickname = from u in userArray
                                   where u.Age > 18
                                   select u.Nickname;

            var majorUsersEmail = userArray.Where(u => u.Age > 18).Select(u => u.Email);

            var majorUsersNickname = userArray.Where(u => u.Age > 18).Select(u => u.Nickname);

            foreach(string x in _majorUsersEmail)
            {
                Console.WriteLine(x);
            }

            NewLine();

            foreach (string x in _majorUsersNickname)
            {
                Console.WriteLine(x);
            }

            NewLine();

            foreach (string x in majorUsersEmail)
            {
                Console.WriteLine(x);
            }

            NewLine();

            foreach (string x in majorUsersNickname)
            {
                Console.WriteLine(x);
            }


            Console.WriteLine("Exercitiu Bonus");

            var areThereTeenageUsers = userArray.Any(s => s.Age > 12 && s.Age < 18);

            Console.WriteLine(areThereTeenageUsers);



            var areAllUsersTeenagers = userArray.All(s => s.Age > 12 && s.Age < 18);

            Console.WriteLine(areAllUsersTeenagers);



            int[] intArray = {0, 1, 2, 3, 4, 5};

            var isThere4 = intArray.Contains(4);

            Console.WriteLine(isThere4);



            var userArrayCount = userArray.Count(s => s.Age < 18);

            Console.WriteLine(userArrayCount);

            

            // sum, max, first, distinct

            var largestElement = intArray.Max();

            var sumOfElements = intArray.Sum();

            var firstElement = intArray.First();

            var distinctArray = intArray.Distinct();

            Console.WriteLine($"Largest Element: {largestElement}");
            Console.WriteLine($"Sum of Elements: {sumOfElements}");
            Console.WriteLine($"First Element: {firstElement}");

            Console.WriteLine($"Distinct Array:");

            foreach (int nr in distinctArray)
            {
                Console.WriteLine($"Distinct Array Element {intArray[nr]}: {nr}");
            }

            int []intArray1 = { 1, 2, 3, 4, 5, 4, 5 };
            int[] intArrayS = { 4, 5, 6, 7, 8 };

            var firstOrDefault = intArray1.FirstOrDefault();

            var lastOrDefault = intArray1.LastOrDefault();

            var average = intArray1.Average();

            var result = intArray1.Except(intArrayS);

            Console.WriteLine($"First Or Default Element: {firstOrDefault}");
            Console.WriteLine($"Last Or Default Element: {lastOrDefault}");
            Console.WriteLine($"Average: {average}");

            NewLine();

            foreach (int nr in result)
            {
                Console.WriteLine($"Except Array Element: {nr}");
            }

        }
    }
}
