﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp2
{
    public class Utilizator
    {
        string nume;
        string prenume;
        int varsta;
        string email;
        long telefon;
        string parola;
        string intrebare;
        string raspuns;

        public Utilizator() { }

        public Utilizator(string nume, string prenume, int varsta, string username, long telefon, string parola, string intrebare, string raspuns)
        {
            this.Nume = nume;
            this.Prenume = prenume;
            this.Varsta = varsta;
            this.Email = email;
            this.Telefon = telefon;
            this.Parola = parola;
            this.Intrebare = intrebare;
            this.Raspuns = raspuns;
        }

        public string Nume { get => nume; set => nume = value; }
        public string Prenume { get => prenume; set => prenume = value; }
        public int Varsta { get => varsta; set => varsta = value; }
        public string Email { get => email; set => email = value; }
        public long Telefon { get => telefon; set => telefon = value; }
        public string Parola { get => parola; set => parola = value; }
        public string Intrebare { get => intrebare; set => intrebare = value; }
        public string Raspuns { get => raspuns; set => raspuns = value; }
    }
}
