﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class MainWindow : Form
    {
        AccountManager manager = new AccountManager();

        public MainWindow()
        {
            InitializeComponent();

            manager.populateListBox(accountsListBox);
        }

        private void accountsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string email = accountsListBox.GetItemText(accountsListBox.SelectedItem);

                Utilizator user = manager.getUserByEmail(email);

                nameTextBox.Text = user.Nume;
                surnameTextBox.Text = user.Prenume;
                usernameTextBox.Text = user.Email;
                passwordTextBox.Text = user.Parola;
                securityQuestionComboBox.Text = user.Intrebare;
                answerTextBox.Text = user.Raspuns;
                phoneTextBox.Text = Convert.ToString(user.Telefon);
                ageTextBox.Text = Convert.ToString(user.Varsta);
            }
            catch
            {
                MessageBox.Show("Could not get user details!");
            }
        }

        private void deleteUserButton_Click(object sender, EventArgs e)
        {
            try
            {
                string email = accountsListBox.GetItemText(accountsListBox.SelectedItem);

                manager.deleteUser(email);

                MessageBox.Show("Successfully deleted user!");

                manager.populateListBox(accountsListBox);
            }
            catch
            {
                MessageBox.Show("Could not delete user!");
            }
        }

        private void saveUserButton_Click(object sender, EventArgs e)
        {
            try
            {
                string email = accountsListBox.GetItemText(accountsListBox.SelectedItem);

                manager.editUser(nameTextBox.Text, surnameTextBox.Text, Convert.ToInt32(ageTextBox.Text), usernameTextBox.Text, Convert.ToInt64(phoneTextBox.Text), passwordTextBox.Text, securityQuestionComboBox.Text, answerTextBox.Text, email);

                MessageBox.Show("Saved user!");
            }
            catch
            {
                MessageBox.Show($"Could not save user!");
            }
        }
    }
}
