﻿
namespace WindowsFormsApp2
{
    partial class RecoverPassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.usernameLabel = new System.Windows.Forms.Label();
            this.usernameTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.securityQuestionComboBox = new System.Windows.Forms.ComboBox();
            this.answerLabel = new System.Windows.Forms.Label();
            this.answerTextBox = new System.Windows.Forms.TextBox();
            this.recoverPasswordButton = new System.Windows.Forms.Button();
            this.signInLinkLabel = new System.Windows.Forms.LinkLabel();
            this.createAccountLinkLabel = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.usernameLabel.Location = new System.Drawing.Point(21, 20);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(87, 23);
            this.usernameLabel.TabIndex = 3;
            this.usernameLabel.Text = "Username";
            // 
            // usernameTextBox
            // 
            this.usernameTextBox.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.usernameTextBox.Location = new System.Drawing.Point(25, 46);
            this.usernameTextBox.Name = "usernameTextBox";
            this.usernameTextBox.Size = new System.Drawing.Size(288, 30);
            this.usernameTextBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.label1.Location = new System.Drawing.Point(21, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 23);
            this.label1.TabIndex = 20;
            this.label1.Text = "Security Question";
            // 
            // securityQuestionComboBox
            // 
            this.securityQuestionComboBox.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.securityQuestionComboBox.FormattingEnabled = true;
            this.securityQuestionComboBox.Items.AddRange(new object[] {
            "Care a fost numele primului tau animal de companie?",
            "Care este numele tau?",
            "Ce culoare a fost primul tau animal de companie?",
            "Care este ziua de nastere a mamei tale?",
            "Care este ziua de nastere a tatalui tau?",
            "Care este culoarea ta preferata?"});
            this.securityQuestionComboBox.Location = new System.Drawing.Point(25, 129);
            this.securityQuestionComboBox.Name = "securityQuestionComboBox";
            this.securityQuestionComboBox.Size = new System.Drawing.Size(288, 31);
            this.securityQuestionComboBox.TabIndex = 19;
            // 
            // answerLabel
            // 
            this.answerLabel.AutoSize = true;
            this.answerLabel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.answerLabel.Location = new System.Drawing.Point(21, 187);
            this.answerLabel.Name = "answerLabel";
            this.answerLabel.Size = new System.Drawing.Size(65, 23);
            this.answerLabel.TabIndex = 22;
            this.answerLabel.Text = "Answer";
            // 
            // answerTextBox
            // 
            this.answerTextBox.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.answerTextBox.Location = new System.Drawing.Point(25, 214);
            this.answerTextBox.Name = "answerTextBox";
            this.answerTextBox.Size = new System.Drawing.Size(288, 30);
            this.answerTextBox.TabIndex = 21;
            // 
            // recoverPasswordButton
            // 
            this.recoverPasswordButton.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.recoverPasswordButton.Location = new System.Drawing.Point(25, 273);
            this.recoverPasswordButton.Name = "recoverPasswordButton";
            this.recoverPasswordButton.Size = new System.Drawing.Size(288, 38);
            this.recoverPasswordButton.TabIndex = 23;
            this.recoverPasswordButton.Text = "Recover Password";
            this.recoverPasswordButton.UseVisualStyleBackColor = true;
            this.recoverPasswordButton.Click += new System.EventHandler(this.recoverPasswordButton_Click);
            // 
            // signInLinkLabel
            // 
            this.signInLinkLabel.AutoSize = true;
            this.signInLinkLabel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.signInLinkLabel.Location = new System.Drawing.Point(21, 329);
            this.signInLinkLabel.Name = "signInLinkLabel";
            this.signInLinkLabel.Size = new System.Drawing.Size(206, 23);
            this.signInLinkLabel.TabIndex = 24;
            this.signInLinkLabel.TabStop = true;
            this.signInLinkLabel.Text = "Already have an account?";
            this.signInLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.signInLinkLabel_LinkClicked);
            // 
            // createAccountLinkLabel
            // 
            this.createAccountLinkLabel.AutoSize = true;
            this.createAccountLinkLabel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.createAccountLinkLabel.Location = new System.Drawing.Point(21, 362);
            this.createAccountLinkLabel.Name = "createAccountLinkLabel";
            this.createAccountLinkLabel.Size = new System.Drawing.Size(191, 23);
            this.createAccountLinkLabel.TabIndex = 25;
            this.createAccountLinkLabel.TabStop = true;
            this.createAccountLinkLabel.Text = "Don\'t have an account?";
            this.createAccountLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.createAccountLinkLabel_LinkClicked);
            // 
            // RecoverPassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 409);
            this.Controls.Add(this.createAccountLinkLabel);
            this.Controls.Add(this.signInLinkLabel);
            this.Controls.Add(this.recoverPasswordButton);
            this.Controls.Add(this.answerLabel);
            this.Controls.Add(this.answerTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.securityQuestionComboBox);
            this.Controls.Add(this.usernameLabel);
            this.Controls.Add(this.usernameTextBox);
            this.Name = "RecoverPassword";
            this.Text = "Recover Password";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.TextBox usernameTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox securityQuestionComboBox;
        private System.Windows.Forms.Label answerLabel;
        private System.Windows.Forms.TextBox answerTextBox;
        private System.Windows.Forms.Button recoverPasswordButton;
        private System.Windows.Forms.LinkLabel signInLinkLabel;
        private System.Windows.Forms.LinkLabel createAccountLinkLabel;
    }
}