﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Controls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace WindowsFormsApp2
{
    public class AccountManager
    {
        private List<Utilizator> returnList()
        {
            List<Utilizator> lu = new List<Utilizator>();
            
            using (var reader = new StreamReader("C:/Users/Ana/Desktop/accounts.csv"))
            {
                while (!reader.EndOfStream)
                {
                    Utilizator utilizator = new Utilizator();

                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    utilizator.Nume = values[0];
                    utilizator.Prenume = values[1];
                    utilizator.Varsta = int.Parse(values[2]);
                    utilizator.Email = values[3];
                    utilizator.Telefon = long.Parse(values[4]);
                    utilizator.Parola = values[5];
                    utilizator.Intrebare = values[6];
                    utilizator.Raspuns = values[7];

                    lu.Add(utilizator);
                }
            }

            return lu;
        }

        private List<Utilizator> returnListJSON()
        {
            List<Utilizator> lu = File.ReadAllLines("C:/Users/Ana/Desktop/accounts.json").Select(line => JsonConvert.DeserializeObject<Utilizator>(line)).ToList();

            return lu;
        }

        public bool checkSignUser(string email, string password)
        {
            List<Utilizator> lu = returnList();

            foreach(Utilizator utilizator in lu)
            {
                if((utilizator.Email.ToLower() == email.ToLower()) && (utilizator.Parola == password))
                {
                    return true;
                }
            }

            return false;
        }

        public Utilizator getUserByEmail(string email)
        {
            List<Utilizator> lu = returnList();

            foreach (Utilizator utilizator in lu)
            {
                if (utilizator.Email.ToLower() == email.ToLower())
                {
                    return utilizator;
                }
            }

            return null;
        }

        public void changePassword(string oldPassword, string newPassword, string username)
        {
            string path = @"C:/Users/Ana/Desktop/accounts.csv";

            List<string> lines = new List<string>();

            if (File.Exists(path))
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    string line;

                    while ((line = reader.ReadLine()) != null)
                    {
                        if (line.Contains(","))
                        {
                            string[] split = line.Split(',');

                            if (split[5].Contains(oldPassword) && split[3].Contains(username))
                            {
                                split[5] = newPassword;

                                line = string.Join(",", split);
                            }
                        }

                        lines.Add(line);
                    }
                }

                using (StreamWriter writer = new StreamWriter(path, false))
                {
                    foreach (string line in lines)
                    {
                        writer.WriteLine(line);
                    }
                }
            }
        }

        public void populateListBox(System.Windows.Forms.ListBox accountsListBox)
        {
            List<Utilizator> lu = returnList();

            accountsListBox.Items.Clear();

            foreach (Utilizator utilizator in lu)
            {
                accountsListBox.Items.Add($"{utilizator.Email}");
            }
        }

        public void deleteUser(string username)
        {
            string path = "C:/Users/Ana/Desktop/accounts.csv";

            List<string> lines = new List<string>();

            if (File.Exists(path))
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    string line;

                    while ((line = reader.ReadLine()) != null)
                    {
                        if (line.Contains(","))
                        {
                            string[] split = line.Split(',');

                            if (split[3].Contains(username))
                            {
                                continue;
                            }
                            else
                            {
                                line = string.Join(",", split);

                                lines.Add(line);
                            }
                        }
                    }
                }

                using (StreamWriter writer = new StreamWriter(path, false))
                {
                    foreach (string line in lines)
                    {
                        writer.WriteLine(line);
                    }
                }
            }
        }

        public void editUser(string nume, string prenume, int varsta, string email, long telefon, string parola, string intrebare, string raspuns, string oldEmail)
        {
            string path = "C:/Users/Ana/Desktop/accounts.csv";

            List<string> lines = new List<string>();

            if (File.Exists(path))
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    string line;

                    while ((line = reader.ReadLine()) != null)
                    {
                        if (line.Contains(","))
                        {
                            string[] split = line.Split(',');

                            if (split[3].Contains(oldEmail))
                            {
                                split[0] = nume;
                                split[1] = prenume;
                                split[2] = Convert.ToString(varsta);
                                split[3] = email;
                                split[4] = Convert.ToString(telefon);
                                split[5] = parola;
                                split[6] = intrebare;
                                split[7] = raspuns;

                                line = string.Join(",", split);
                            }
                        }

                        lines.Add(line);
                    }
                }

                using (StreamWriter writer = new StreamWriter(path, false))
                {
                    foreach (string line in lines)
                    {
                        writer.WriteLine(line);
                    }
                }
            }
        }

        public void createAccount(string name, string surname, int age, string username, long phone, string password, string securityQuestion, string answer)
        {
            if (File.ReadAllText("C:/Users/Ana/Desktop/accounts.csv").Contains(username))
            {
                System.Windows.Forms.MessageBox.Show("Username already in use!");
            }
            else
            {
                try
                {
                    string newLine = $"\n{name},{surname},{age},{username},{phone},{password},{securityQuestion},{answer}";

                    File.AppendAllText("C:/Users/Ana/Desktop/accounts.csv", newLine);

                    System.Windows.Forms.MessageBox.Show("Successfully created your account!");
                }
                catch
                {
                    System.Windows.Forms.MessageBox.Show("Something went wrong whilst creating your account!");
                }
            }
        }
    
        public void createAccountJSON(string name, string surname, int age, string username, long phone, string password, string securityQuestion, string answer)
        {
                
            Utilizator data = new Utilizator()
            {
                Nume = name,
                Prenume = surname,
                Varsta = age,
                Email = username,
                Telefon = phone,
                Parola = password,
                Intrebare = securityQuestion,
                Raspuns = answer
            };

            string json = JsonConvert.SerializeObject(data);

            System.IO.File.AppendAllText("C:/Users/Ana/Desktop/accounts.json", $"\n{json}");
        }

        public bool checkSignInUserJSON(string email, string password)
        {
            var objs = returnListJSON();

            return objs.Any(x => x.Email == email && x.Parola == password);
        }

        public Utilizator getUserByEmailJSON(string email)
        {
            var objs = returnListJSON();

            return objs.FirstOrDefault(u => u.Email == email);
        }
    }
}
