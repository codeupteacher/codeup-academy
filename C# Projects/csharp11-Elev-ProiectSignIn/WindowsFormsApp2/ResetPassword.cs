﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class ResetPassword : Form
    {
        AccountManager manager = new AccountManager();

        public ResetPassword()
        {
            InitializeComponent();
        }

        private void changePasswordButton_Click(object sender, EventArgs e)
        {
            try
            {
                var result = manager.checkSignUser(usernameTextBox.Text, passwordTextBox.Text);

                if (newPasswordTextBox.Text != confirmNewPasswordTextBox.Text)
                {
                    MessageBox.Show("New passwords do not match!");
                }
                else if (!result)
                {
                    MessageBox.Show("Account information is invalid!");
                }
                else if (passwordTextBox.Text == newPasswordTextBox.Text)
                {
                    MessageBox.Show("New password cannot be the same as your old password!");
                }
                else if (result && newPasswordTextBox.Text == confirmNewPasswordTextBox.Text)
                {
                    manager.changePassword(passwordTextBox.Text, newPasswordTextBox.Text, usernameTextBox.Text);

                    MessageBox.Show("Successfully changed password!");

                    SignIn si = new SignIn();

                    si.Show();

                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Invalid information entered!");
                }
            }
            catch
            {
                MessageBox.Show("Something went wrong whilst resetting your password!");
            }
        }

        private void forgotPasswordLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            SignIn si = new SignIn();

            si.Show();

            this.Hide();
        }
    }
}
