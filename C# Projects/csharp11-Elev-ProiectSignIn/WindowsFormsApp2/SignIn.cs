﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class SignIn : Form
    {
        AccountManager manager = new AccountManager();
        public SignIn()
        {
            InitializeComponent();
        }

        private void createAccountButton_Click(object sender, EventArgs e)
        {
            CreateAccount ca = new CreateAccount();

            ca.Show();

            this.Hide();
        }

        private void forgotPasswordLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            RecoverPassword rp = new RecoverPassword();

            rp.Show();

            this.Hide();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ResetPassword rp = new ResetPassword();

            rp.Show();

            this.Hide();
        }

        private void signInButton_Click(object sender, EventArgs e)
        {
            try
            {
                bool result = manager.checkSignInUserJSON(usernameTextBox.Text, passwordTextBox.Text);

                if (result)
                {
                    MessageBox.Show("Successfully signed in!");

                    MainWindow mw = new MainWindow();

                    mw.Show();

                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Wrong email or password!");
                }
            }
            catch
            {
                MessageBox.Show("Something went wrong whilst signing in!");
            }
        }
    }
}
