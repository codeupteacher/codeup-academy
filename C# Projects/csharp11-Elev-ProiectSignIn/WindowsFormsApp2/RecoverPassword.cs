﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class RecoverPassword : Form
    {
        AccountManager manager = new AccountManager();

        public RecoverPassword()
        {
            InitializeComponent();
        }

        private void signInLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            SignIn si = new SignIn();

            si.Show();

            this.Hide();
        }

        private void createAccountLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            CreateAccount ca = new CreateAccount();

            ca.Show();

            this.Hide();
        }

        private void recoverPasswordButton_Click(object sender, EventArgs e)
        {
            Utilizator utilizator = manager.getUserByEmailJSON(usernameTextBox.Text);

            try
            {
                if (securityQuestionComboBox.Text == utilizator.Intrebare && answerTextBox.Text.ToLower() == utilizator.Raspuns.ToLower())
                {
                    MessageBox.Show(utilizator.Parola);
                }
                else
                {
                    MessageBox.Show("Wrong question or answer!");
                }
            }
            catch
            {
                MessageBox.Show("Something went wrong whilst recovering your password!");
            }
        }
    }
}
