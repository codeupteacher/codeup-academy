﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class CreateAccount : Form
    {
        AccountManager manager = new AccountManager();

        public CreateAccount()
        {
            InitializeComponent();
        }

        private void signInButton_Click(object sender, EventArgs e)
        {
            SignIn si = new SignIn();

            si.Show();

            this.Hide();
        }

        private void createAccountButton_Click(object sender, EventArgs e)
        {
            try
            {
                manager.createAccountJSON(nameTextBox.Text, surnameTextBox.Text, Convert.ToInt32(ageTextBox.Text), usernameTextBox.Text, Convert.ToInt64(phoneTextBox.Text), passwordTextBox.Text, securityQuestionComboBox.Text, answerTextBox.Text);

                MessageBox.Show("Successfully created account!");

                SignIn si = new SignIn();

                si.Show();

                this.Hide();
            }
            catch
            {
                MessageBox.Show("Your account could not be created!");
            }
        }
    }
}
