﻿
namespace WindowsFormsApp2
{
    partial class ResetPassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.newPasswordText = new System.Windows.Forms.Label();
            this.newPasswordTextBox = new System.Windows.Forms.TextBox();
            this.confirmNewPasswordText = new System.Windows.Forms.Label();
            this.confirmNewPasswordTextBox = new System.Windows.Forms.TextBox();
            this.changePasswordButton = new System.Windows.Forms.Button();
            this.passwordText = new System.Windows.Forms.Label();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.usernameText = new System.Windows.Forms.Label();
            this.usernameTextBox = new System.Windows.Forms.TextBox();
            this.forgotPasswordLabel = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // newPasswordText
            // 
            this.newPasswordText.AutoSize = true;
            this.newPasswordText.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.newPasswordText.Location = new System.Drawing.Point(22, 174);
            this.newPasswordText.Name = "newPasswordText";
            this.newPasswordText.Size = new System.Drawing.Size(119, 23);
            this.newPasswordText.TabIndex = 5;
            this.newPasswordText.Text = "New Password";
            // 
            // newPasswordTextBox
            // 
            this.newPasswordTextBox.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.newPasswordTextBox.Location = new System.Drawing.Point(26, 200);
            this.newPasswordTextBox.Name = "newPasswordTextBox";
            this.newPasswordTextBox.PasswordChar = '*';
            this.newPasswordTextBox.Size = new System.Drawing.Size(288, 30);
            this.newPasswordTextBox.TabIndex = 4;
            // 
            // confirmNewPasswordText
            // 
            this.confirmNewPasswordText.AutoSize = true;
            this.confirmNewPasswordText.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.confirmNewPasswordText.Location = new System.Drawing.Point(22, 253);
            this.confirmNewPasswordText.Name = "confirmNewPasswordText";
            this.confirmNewPasswordText.Size = new System.Drawing.Size(185, 23);
            this.confirmNewPasswordText.TabIndex = 7;
            this.confirmNewPasswordText.Text = "Confirm New Password";
            // 
            // confirmNewPasswordTextBox
            // 
            this.confirmNewPasswordTextBox.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.confirmNewPasswordTextBox.Location = new System.Drawing.Point(26, 279);
            this.confirmNewPasswordTextBox.Name = "confirmNewPasswordTextBox";
            this.confirmNewPasswordTextBox.PasswordChar = '*';
            this.confirmNewPasswordTextBox.Size = new System.Drawing.Size(288, 30);
            this.confirmNewPasswordTextBox.TabIndex = 6;
            // 
            // changePasswordButton
            // 
            this.changePasswordButton.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.changePasswordButton.Location = new System.Drawing.Point(26, 335);
            this.changePasswordButton.Name = "changePasswordButton";
            this.changePasswordButton.Size = new System.Drawing.Size(288, 38);
            this.changePasswordButton.TabIndex = 24;
            this.changePasswordButton.Text = "Change Password";
            this.changePasswordButton.UseVisualStyleBackColor = true;
            this.changePasswordButton.Click += new System.EventHandler(this.changePasswordButton_Click);
            // 
            // passwordText
            // 
            this.passwordText.AutoSize = true;
            this.passwordText.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.passwordText.Location = new System.Drawing.Point(22, 97);
            this.passwordText.Name = "passwordText";
            this.passwordText.Size = new System.Drawing.Size(112, 23);
            this.passwordText.TabIndex = 26;
            this.passwordText.Text = "Old Password";
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.passwordTextBox.Location = new System.Drawing.Point(26, 123);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.PasswordChar = '*';
            this.passwordTextBox.Size = new System.Drawing.Size(288, 30);
            this.passwordTextBox.TabIndex = 25;
            // 
            // usernameText
            // 
            this.usernameText.AutoSize = true;
            this.usernameText.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.usernameText.Location = new System.Drawing.Point(22, 18);
            this.usernameText.Name = "usernameText";
            this.usernameText.Size = new System.Drawing.Size(87, 23);
            this.usernameText.TabIndex = 28;
            this.usernameText.Text = "Username";
            // 
            // usernameTextBox
            // 
            this.usernameTextBox.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.usernameTextBox.Location = new System.Drawing.Point(26, 44);
            this.usernameTextBox.Name = "usernameTextBox";
            this.usernameTextBox.Size = new System.Drawing.Size(288, 30);
            this.usernameTextBox.TabIndex = 27;
            // 
            // forgotPasswordLabel
            // 
            this.forgotPasswordLabel.AutoSize = true;
            this.forgotPasswordLabel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.forgotPasswordLabel.Location = new System.Drawing.Point(22, 389);
            this.forgotPasswordLabel.Name = "forgotPasswordLabel";
            this.forgotPasswordLabel.Size = new System.Drawing.Size(206, 23);
            this.forgotPasswordLabel.TabIndex = 29;
            this.forgotPasswordLabel.TabStop = true;
            this.forgotPasswordLabel.Text = "Already have an account?";
            this.forgotPasswordLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.forgotPasswordLabel_LinkClicked);
            // 
            // ResetPassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 435);
            this.Controls.Add(this.forgotPasswordLabel);
            this.Controls.Add(this.usernameText);
            this.Controls.Add(this.usernameTextBox);
            this.Controls.Add(this.passwordText);
            this.Controls.Add(this.passwordTextBox);
            this.Controls.Add(this.changePasswordButton);
            this.Controls.Add(this.confirmNewPasswordText);
            this.Controls.Add(this.confirmNewPasswordTextBox);
            this.Controls.Add(this.newPasswordText);
            this.Controls.Add(this.newPasswordTextBox);
            this.Name = "ResetPassword";
            this.Text = "Reset Password";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label newPasswordText;
        private System.Windows.Forms.TextBox newPasswordTextBox;
        private System.Windows.Forms.Label confirmNewPasswordText;
        private System.Windows.Forms.TextBox confirmNewPasswordTextBox;
        private System.Windows.Forms.Button changePasswordButton;
        private System.Windows.Forms.Label passwordText;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.Label usernameText;
        private System.Windows.Forms.TextBox usernameTextBox;
        private System.Windows.Forms.LinkLabel forgotPasswordLabel;
    }
}