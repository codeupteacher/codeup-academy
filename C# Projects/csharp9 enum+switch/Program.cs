﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teme_csharp9
{
    public class Program
    {
        public enum Vocale
        {
            NotSpecified,
            a,
            e,
            i,
            o,
            u
        }
        public static void Main(String[] args)
        {

            MultiplyStringByLetter("j");
            GetVocalaInSwitchByInt(Vocale.u);
        }

        public static void MultiplyStringByLetter(string litera)
        {

            Enum.TryParse(litera, out Vocale vocalaprimita);
            
            switch (vocalaprimita)
            {
                case Vocale.a:
                    Console.WriteLine("aaa");
                    break;
                case Vocale.e:
                    Console.WriteLine("eee");
                    break;
                case Vocale.i:
                    Console.WriteLine("iii");
                    break;
                case Vocale.o:
                    Console.WriteLine("oooo");
                    break;
                case Vocale.u:
                    Console.WriteLine("uuu");
                    break;
                case Vocale.NotSpecified:
                default:
                    Console.WriteLine("Nu este vocala");
                    break;
            }
        }


        public static void SwitchInt(int cifra)
        {
            switch (cifra)
            {
                case 0:
                    Console.WriteLine(cifra.ToString() + "este 0");
                    break;
                case < 0 :
                    Console.WriteLine(cifra.ToString() + "este mai mica 0");
                    break;
                case var expression when cifra > 0:
                    Console.WriteLine(cifra.ToString() + "este mare 0");
                    break;
            }
        }

        public static void GetVocalaInSwitchByInt(Vocale voc)
        {
            var intVocala = (int)voc;

            switch (intVocala)
            {
                case 0:
                    Console.WriteLine("Not Specified");
                    break;
                case 1:
                    Console.WriteLine("aaa");
                    break;
                case 2:
                    Console.WriteLine("eeee");
                    break;
                case 3:
                    Console.WriteLine("iii");
                    break;
                case 4:
                    Console.WriteLine("oooo");
                    break;
                case 5:
                    Console.WriteLine("uuu");
                    break;
            }
        }
    }
}
