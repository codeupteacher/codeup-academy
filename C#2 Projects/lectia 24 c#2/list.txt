using System;
using System.Collections.Generic;

class Program
{
    static void Main()
    {
        List<string> words = new List<string> { "banana", "apple", "orange", "grape" };

        words.Sort();
        words.Reverse();

        Console.WriteLine("Sorted list of strings in descending order:");
        foreach (string word in words)
        {
            Console.Write(word + " ");
        }
    }
}