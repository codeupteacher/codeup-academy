﻿using System;

namespace lectia_4
{
    class Program
    {
        public static void exchange(int[] data, int m, int n)
        {
            int temporary;

            temporary = data[m];
            data[m] = data[n];
            data[n] = temporary;
        }

        public static void IntArrayBubbleSort(int[] data)
        {
            int i, j;
            int N = data.Length;

            for (j = N - 1; j > 0; j--)
            {
                for (i = 0; i < j; i++)
                {
                    if (data[i] > data[i + 1])
                        exchange(data, i, i + 1);
                }
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Nr de elemente ale vectorului: ");

            int n = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Introdu el ementele vectorului: ");

            int[] a = new int[n];

            for (int i = 0; i < n; i++)
            {
                a[i] = Convert.ToInt32(Console.ReadLine());
            }


            //ex 2
            Console.WriteLine("");

            IntArrayBubbleSort(a);

            foreach(var elem in a)
            {
                if (elem % 2 == 1)
                    Console.WriteLine(elem + " ");
            }

            //ex 3
            Console.WriteLine("");

            for (int i = 0; i < n / 2 - 1; i++)
                for (int j = i + 1; j < n / 2; j++)
                    if (a[i] > a[j])
                        exchange(a,i,j);
            for (int i = n / 2 + 1; i < n - 1; i++)
                for (int j = i + 1; j < n; j++)
                    if (a[i] < a[j])
                        exchange(a,i,j);

 

            foreach (var elem in a)
            {
                Console.WriteLine(elem + " ");
            }

            //ex 4
            Console.WriteLine("");
            int max = a[0];
            int pos = 0;
            for (int i = 1; i < n; i++)
            {
                if ( max < a[i])
                {
                    max = a[i];
                    pos = i;
                }
            }

            for (int i = 0; i < pos; i++)
                for (int j = i + 1; j < n / 2; j++)
                    if (a[i] > a[j])
                        exchange(a, i, j);
            for (int i = pos + 1; i < n - 1; i++)
                for (int j = i + 1; j < n; j++)
                    if (a[i] < a[j])
                        exchange(a, i, j);

            foreach (var elem in a)
            {
                Console.WriteLine(elem + " ");
            }

        }
    }
}
