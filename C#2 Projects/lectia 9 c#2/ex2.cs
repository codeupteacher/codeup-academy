using System;
using System.Collections.Generic;

namespace lectia9
{
    class Program
    {
        static void Main(string[] args)
        {

            // ex 2

            Stack<int> stack = new Stack<int>();
            stack.Push(10);
            stack.Push(-5);
            stack.Push(1);
            stack.Push(-8);
            stack.Push(15);

            stack = Sort(stack);

            //Item
            Console.WriteLine("Sorted stack:");
            while (stack.Count > 0)
            {
                Console.WriteLine(stack.Pop());
            }

            Console.ReadLine();
        }

        static Stack<int> Sort(Stack<int> stack)
        {
            Stack<int> temp = new Stack<int>();
            //Until all elements are moved
            while (stack.Count > 0)
            {
                var element = stack.Pop();

                //If peek element in temp stack is grater than item we want to insert
                //Pop elements from temp and push in stack until we have correct position for element
                while (temp.Count > 0 && element < temp.Peek())
                {
                    stack.Push(temp.Pop());
                }
                //Push element at its right position
                temp.Push(element);
            }
            return temp;
        }
    }
}
