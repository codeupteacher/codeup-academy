using System;
using System.IO;

class Program
{
    static void Main(string[] args)
    {
        // n is the number of vertices
        // m is the number of edges
        int n, m;
        n = Convert.ToInt32(Console.ReadLine());
        m = Convert.ToInt32(Console.ReadLine());

        int[,] adjMat = new int[n + 1, n + 1];

        for (int i = 0; i < m; i++)
        {
            int u, v;
            u = Convert.ToInt32(Console.ReadLine());
            v = Convert.ToInt32(Console.ReadLine());

            adjMat[u, v] = 1;
            adjMat[v, u] = 1;
            // for a directed graph with an edge pointing
            // from u to v,we just assign adjMat[u][v] as 1
        }
    }
}