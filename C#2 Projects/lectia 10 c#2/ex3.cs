using System;
using System.Collections;
 
class GfG {
 
    public class Stack {
        // Two inbuilt queues
        public Queue q1 = new Queue();
        public Queue q2 = new Queue();
 
        public void push(int x)
        {
            // Push x first in empty q2
            q2.Enqueue(x);
 
            // Push all the remaining
            // elements in q1 to q2.
            while (q1.Count > 0) {
                q2.Enqueue(q1.Peek());
                q1.Dequeue();
            }
 
            // swap the names of two queues
            Queue q = q1;
            q1 = q2;
            q2 = q;
        }
 
        public void pop()
        {
 
            // if no elements are there in q1
            if (q1.Count == 0)
                return;
            q1.Dequeue();
        }
 
        public int top()
        {
            if (q1.Count == 0)
                return -1;
            return (int)q1.Peek();
        }
 
        public int size() { return q1.Count; }
    };
 
    // Driver code
    public static void Main(String[] args)
    {
        Stack s = new Stack();
        s.push(1);
        s.push(2);
        s.push(3);
        Console.WriteLine("current size: " + s.size());
        Console.WriteLine(s.top());
        s.pop();
        Console.WriteLine(s.top());
        s.pop();
        Console.WriteLine(s.top());
        Console.WriteLine("current size: " + s.size());
    }
}