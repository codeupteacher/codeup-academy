//easy way

int[] nums = { 1, 2, 3, 4, 3, 55, 23, 2 };
int[] dist = nums.Distinct().ToArray();

//other way (copy elements in list and convert list to array

            string[] sArray = { "a", "b", "b", "c", "c", "d", "e", "f", "f" };
            List<string> sList = new List<string>();

            for (int i = 0; i < sArray.Length; i++)
            {
                if (sList.Contains(sArray[i]) == false)
                {
                    sList.Add(sArray[i]);
                }
            }

            var sNew = sList.ToArray();

            for (int i = 0; i < sNew.Length; i++)
            {
                Console.Write(sNew[i]);
            }