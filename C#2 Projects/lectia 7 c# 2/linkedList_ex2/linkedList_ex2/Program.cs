﻿using System;

namespace linkedList_ex2
{
    class Program
    {

        public class Node
        {
            //TODO 1
            public int data;
            public Node next;
        }


        //Afisam elementele incepand cu primul
        static void print_linkedlist(Node start)
        {
            //TODO 2
            if (start == null)
            {
                return;
            }
            Console.Write(start.data + " ");

            if (start.next != null)
            {
                print_linkedlist(start.next);
            }
        }

        //lectia 7 ex 1 sterge ultimul element

        static Node removeLastNode(Node head)
        {
            if (head == null)
                return null;

            if (head.next == null)
            {
                return null;
            }

            // Find the second last node
            Node second_last = head;
            while (second_last.next.next != null)
                second_last = second_last.next;

            // Change next of second last
            second_last.next = null;

            return head;
        }

        //lectia 7 ex 2 sterge la k nod de la coada la cap

        static void deleteKthNodeFromEnd(Node head, int k)
        {

            if (head == null || k == 0)
            {
                return;
            }

            // [a, b, c, d]
            // k = 2
            Node first = head;
            Node second = head;

            for (int i = 0; i < k; i++)
            {
                second = second.next;
                if (second.next == null)
                {
                    // K >= Length of List
                    if (i == k - 1)
                    {
                        head = head.next;
                    }
                    return;
                }
            }

            // second = c

            while (second.next != null)
            {
                first = first.next;
                second = second.next;
            }

            // first = b
            // second = d

            // [a, b, c, d]
            // k = 2

            first.next = first.next.next;
        }


        //primeste head si informatia pentru noul nod
        //si il pune pe acesta inainte de head si il face head
        static Node Push(Node head, int new_data)
        {
            //Declaram un obiect de tipul nod
            Node new_node = new Node();

            //salvat informatiile in noul nod
            new_node.data = new_data;

            //legam noul nod de head, il pozitiona inaintea head ului
            new_node.next = head;

            //mutam head ul sa fie noul nod si il returnam
            head = new_node;
            return head;
        }

        public static void Main(String[] args)
        {
            //initializam primul nod , head cu null
            Node head = null;

            // construim lista 2->4->6->8->10 folosind functia push
            head = Push(head, 10);
            head = Push(head, 8);
            head = Push(head, 6);
            head = Push(head, 4);
            head = Push(head, 2);

            Console.Write("Lista are elementele 2->4->6->8->10\n");
            print_linkedlist(head);

            deleteKthNodeFromEnd(head, 2);

            print_linkedlist(head);



        }
    }
}
