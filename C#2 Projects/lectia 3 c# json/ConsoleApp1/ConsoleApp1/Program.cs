﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.Json;

namespace ConsoleApp1
{
    class Program
    {
        public static object JsonConvert { get; private set; }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");


            List<string> marimi = new List<string>() { "Mare", "Mic", "Foarte", "mare" };

            string json = JsonSerializer.Serialize<List<string>>(marimi);
            List<string> ListaDeserializata = JsonSerializer.Deserialize<List<string>>(json);

            Console.WriteLine(json);

            //ex 2
            List<int> valori = new List<int>() { 2, 4, 7, 12};

            var json2 = JsonSerializer.Serialize(valori);

            Console.WriteLine(json2);

            //ex 3

            int[] array1 = new int[] { 1, 3, 5, 7, 9 };

            var json3 = JsonSerializer.Serialize(array1);

            Console.WriteLine(json3);






        }
    }
}
