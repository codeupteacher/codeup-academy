using System;
using System.Collections.Generic;

public class Program
{
    public static void Main()
    {
        List<string> myList = new List<string> { "A", "A", "B", "B" };

        HashSet<string> myHashset = new HashSet<string>(myList);

        foreach (string item in myHashset)
            Console.WriteLine(item);
    }
}